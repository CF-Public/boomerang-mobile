package com.comfithealth.library.codec

import com.comfithealth.library.common.getLong
import com.comfithealth.library.util.byteArrayOfInts
import kotlin.test.Test
import kotlin.test.assertEquals

class DateTimeParserTest {
    @Test
    fun `parsing works`() {
        val input = byteArrayOfInts(0x3e, 0x5c, 0xa7, 0xa5, 0x7c, 0x02, 0x00, 0x00)
        val timestamp = input.getLong()
        val expected = 2734378409022L
        assertEquals(expected, timestamp)
    }
}