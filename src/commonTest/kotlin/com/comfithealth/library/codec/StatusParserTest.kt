package com.comfithealth.library.codec

import com.comfithealth.library.models.*
import com.comfithealth.library.util.byteArrayOfInts
import kotlin.test.Test
import kotlin.test.assertEquals

class StatusParserTest {
    @Test
    fun `parsing works`() {
        val input = byteArrayOfInts(0x10, 0x00)
        val expected = StatusParser.Result(
            status = Status(
                value = input,
                state = DeviceState.READY,
                sampleRate = SampleRate.R_256,
                lpFilter = LPFilter.Off,
                mode = Mode.Manual,
                fingers = FingerMode.Three,
                isDSPOn = true,
                finishCode = null,
                boomerangErr = null
            ),
            extendedError = false
        )
        val output = StatusParser.parse(input)
        assertEquals(expected, output)
    }
}