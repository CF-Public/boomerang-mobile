#include <algorithm>
#include <cstdio>

extern "C" {
#include <codegen/db5lev5.h>
#include <codegen/rt_nonfinite.h>
}

#include <utility>
#include <xtensor/xadapt.hpp>
#include <xtensor/xarray.hpp>

#include "dspkit.hpp"
#include "octave.hpp"
#include "quality.hpp"


static const int WINDOW_SIZE = 6400;
static double input_buff[WINDOW_SIZE];
static creal_T output_buff[WINDOW_SIZE];
static double cxd[38400];
double thr[5];

/*
 * Some MATLAB CodeGen stuff
 * Arguments    : void
 * Return Type  : char
 */
static inline char argInit_char_T() { return '?'; }

//void dspkit::fft(const int16_t *input, double *output, uint32_t size) {
//    return rfft(input, output, size);
//}

dspkit::dspkit() {
    db5lev5_initialize();
}

std::vector<double> dspkit::filter(std::vector<std::int16_t> input) {
    auto length = input.size();
    std::vector<double> output(length);
    int offset = 0;

    while (offset < length) {
        // prepare input
        for (int i = 0; i < WINDOW_SIZE; i++) {
            input_buff[i] = (double) input[(i + offset) % length];
        }

        // filter
        db5lev5(input_buff, argInit_char_T(), output_buff, cxd, thr);

        // prepare output
        for (int i = 0; i < WINDOW_SIZE && (i + offset) < length; i++) {
            output[i + offset] = output_buff[i].re;
        }
        offset += WINDOW_SIZE;
    }
    return output;
}

std::shared_ptr<dsp_result> dspkit::analyze(std::vector<std::int16_t> input) {
    auto output = this->filter(std::move(input));
    auto is_noisy = isNoisy(output);
    return std::make_shared<dsp_result>(dsp_result(output, is_noisy));
}
