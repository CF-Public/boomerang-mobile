#ifndef dsp_kit_util_h
#define dsp_kit_util_h

#include <string>
#include <cstdarg>
#include <cstdint>
#include <vector>
#include <exception>

class DSPKitException : public std::exception {
public:
    const char *msg;

    DSPKitException(const char *msg) : msg(msg) {}

    const char *what() const noexcept override {
        return msg;
    }
};

void platform_log_vector(const std::vector<double> &arr, int length);

void platform_log_short_array(const int16_t *arr, int length);

void platform_log_double_array(double *arr, int length);

#endif /* dsp_kit_util_h */