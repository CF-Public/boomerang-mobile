#ifndef quality_hpp
#define quality_hpp

#include <stdio.h>

#include "stddef.h"

bool isNoisy(const std::vector<double> &signal);

#endif /* quality_hpp */
