#ifndef dspkit_hpp
#define dspkit_hpp

#include <cstdint>
#include <vector>

#include <scapix/bridge/object.h>

#include "dsp_result.hpp"

#ifdef __cplusplus
extern "C" {
#endif

class dspkit : public scapix::bridge::object<dspkit> {
public:
    dspkit();

    std::vector<double> filter(std::vector<std::int16_t> input);

    std::shared_ptr<dsp_result> analyze(std::vector<std::int16_t> input);
};

#ifdef __cplusplus
}
#endif
#endif // dspkit_hpp
