/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: db5lev5.c
 *
 * MATLAB Coder version            : 4.2
 * C/C++ source code generated on  : 28-Feb-2020 16:40:56
 */

/* Include Files */
#include <math.h>
#include "rt_nonfinite.h"
#include <string.h>
#include "db5lev5.h"

/* Variable Definitions */
static const double dv0[10] = { 0.11320949129173116, 0.42697177135271441,
  0.51216347213016522, 0.097883480673751008, -0.17132835769133176,
  -0.022800565942050228, 0.054851329321080562, -0.0044134000543300453,
  -0.00889593505093009, 0.0023587139692000242 };

static const double dv1[10] = { 0.0023587139692000242, 0.00889593505093009,
  -0.0044134000543300453, -0.054851329321080562, -0.022800565942050228,
  0.17132835769133176, 0.097883480673751008, -0.51216347213016522,
  0.42697177135271441, -0.11320949129173116 };

/* Function Declarations */
static void b_abs(const double x[6400], double y[6400]);
static void b_fft(const double x[6400], creal_T y[6400]);
static void b_mod(const double x[6400], double r[6400]);
static void b_r2br_r2dit_trig(const creal_T x[16384], const double costab[8193],
  const double sintab[8193], creal_T y[16384]);
static void b_sign(double x[6400]);
static void b_sqrt(double *x);
static void bluesteinSetup(creal_T wwc[12799]);
static void bluestein_setup_impl(creal_T wwc[12799]);
static void c_sqrt(double x[5]);
static void fft(const double x[10], creal_T y[6400]);
static void generate_twiddle_tables(double costab[8193], double sintab[8193],
  double sintabinv[8193]);
static void ifft(const creal_T x[6400], creal_T y[6400]);
static void imodwt5(const double w[38400], creal_T xrec[6400]);
static void imodwtrec(const double Vin[6400], const double Win[6400], const
                      creal_T G[6400], const creal_T H[6400], double J, creal_T
                      Vout[6400]);
static void med3(double v[6400], int nv, int ia, int ib);
static double median(const double x[6400]);
static void medmed(double v[6400], int nv, int ia);
static void modwt5(const double x[6400], double w[38400]);
static void modwtdec(const creal_T X[6400], const creal_T G[6400], const creal_T
                     H[6400], double J, creal_T Vhat[6400], creal_T What[6400]);
static double mpower(double a, double b);
static int pivot(double v[6400], int *ip, int ia, int ib);
static void power(const double a[5], double y[5]);
static void quickselect(double v[6400], int n, int idx0, int vlen, double *vn,
  int *nfirst, int *nlast);
static void r2br_r2dit_trig(const creal_T x[12799], const double costab[8193],
  const double sintab[8193], creal_T y[16384]);
static void r2br_r2dit_trig_impl(const creal_T x[6400], int xoffInit, const
  double costab[8193], const double sintab[8193], creal_T y[16384]);
static double rt_powd_snf(double u0, double u1);
static int thirdOfFive(const double v[6400], int ia, int ib);
static void wthresh(const double x[6400], char sorh, double t, double y[6400]);

/* Function Definitions */

/*
 * Arguments    : const double x[6400]
 *                double y[6400]
 * Return Type  : void
 */
static void b_abs(const double x[6400], double y[6400])
{
  int k;
  for (k = 0; k < 6400; k++) {
    y[k] = fabs(x[k]);
  }
}

/*
 * Arguments    : const double x[6400]
 *                creal_T y[6400]
 * Return Type  : void
 */
static void b_fft(const double x[6400], creal_T y[6400])
{
  static double costab[8193];
  static double sintab[8193];
  static double sintabinv[8193];
  static creal_T wwc[12799];
  int xidx;
  int k;
  static creal_T fv[16384];
  static creal_T b_fv[16384];
  double fv_re;
  generate_twiddle_tables(costab, sintab, sintabinv);
  bluestein_setup_impl(wwc);
  xidx = 0;
  for (k = 0; k < 6400; k++) {
    y[k].re = wwc[k + 6399].re * x[xidx];
    y[k].im = wwc[k + 6399].im * -x[xidx];
    xidx++;
  }

  r2br_r2dit_trig_impl(y, 0, costab, sintab, fv);
  r2br_r2dit_trig(wwc, costab, sintab, b_fv);
  for (xidx = 0; xidx < 16384; xidx++) {
    fv_re = b_fv[xidx].re;
    b_fv[xidx].re = fv[xidx].re * b_fv[xidx].re - fv[xidx].im * b_fv[xidx].im;
    b_fv[xidx].im = fv[xidx].re * b_fv[xidx].im + fv[xidx].im * fv_re;
  }

  b_r2br_r2dit_trig(b_fv, costab, sintabinv, fv);
  xidx = 0;
  for (k = 0; k < 6400; k++) {
    y[xidx].re = wwc[k + 6399].re * fv[k + 6399].re + wwc[k + 6399].im * fv[k +
      6399].im;
    y[xidx].im = wwc[k + 6399].re * fv[k + 6399].im - wwc[k + 6399].im * fv[k +
      6399].re;
    xidx++;
  }
}

/*
 * Arguments    : const double x[6400]
 *                double r[6400]
 * Return Type  : void
 */
static void b_mod(const double x[6400], double r[6400])
{
  int k;
  double b_r;
  for (k = 0; k < 6400; k++) {
    if (rtIsNaN(x[k]) || rtIsInf(x[k])) {
      b_r = rtNaN;
    } else if (x[k] == 0.0) {
      b_r = 0.0;
    } else {
      b_r = fmod(x[k], 6400.0);
      if (b_r == 0.0) {
        b_r = 0.0;
      } else {
        if (x[k] < 0.0) {
          b_r += 6400.0;
        }
      }
    }

    r[k] = b_r;
  }
}

/*
 * Arguments    : const creal_T x[16384]
 *                const double costab[8193]
 *                const double sintab[8193]
 *                creal_T y[16384]
 * Return Type  : void
 */
static void b_r2br_r2dit_trig(const creal_T x[16384], const double costab[8193],
  const double sintab[8193], creal_T y[16384])
{
  int ix;
  int ju;
  int iy;
  int i;
  bool tst;
  double temp_re;
  double temp_im;
  double twid_re;
  int iheight;
  double twid_im;
  int istart;
  int temp_re_tmp;
  int j;
  int ihi;
  ix = 0;
  ju = 0;
  iy = 0;
  for (i = 0; i < 16383; i++) {
    y[iy] = x[ix];
    iy = 16384;
    tst = true;
    while (tst) {
      iy >>= 1;
      ju ^= iy;
      tst = ((ju & iy) == 0);
    }

    iy = ju;
    ix++;
  }

  y[iy] = x[ix];
  for (i = 0; i <= 16382; i += 2) {
    temp_re = y[i + 1].re;
    temp_im = y[i + 1].im;
    twid_re = y[i].re;
    twid_im = y[i].im;
    y[i + 1].re = y[i].re - y[i + 1].re;
    y[i + 1].im = y[i].im - y[i + 1].im;
    twid_re += temp_re;
    twid_im += temp_im;
    y[i].re = twid_re;
    y[i].im = twid_im;
  }

  iy = 2;
  ix = 4;
  ju = 4096;
  iheight = 16381;
  while (ju > 0) {
    for (i = 0; i < iheight; i += ix) {
      temp_re_tmp = i + iy;
      temp_re = y[temp_re_tmp].re;
      temp_im = y[i + iy].im;
      y[temp_re_tmp].re = y[i].re - y[temp_re_tmp].re;
      y[temp_re_tmp].im = y[i].im - temp_im;
      y[i].re += temp_re;
      y[i].im += temp_im;
    }

    istart = 1;
    for (j = ju; j < 8192; j += ju) {
      twid_re = costab[j];
      twid_im = sintab[j];
      i = istart;
      ihi = istart + iheight;
      while (i < ihi) {
        temp_re_tmp = i + iy;
        temp_re = twid_re * y[temp_re_tmp].re - twid_im * y[i + iy].im;
        temp_im = twid_re * y[i + iy].im + twid_im * y[i + iy].re;
        y[temp_re_tmp].re = y[i].re - temp_re;
        y[temp_re_tmp].im = y[i].im - temp_im;
        y[i].re += temp_re;
        y[i].im += temp_im;
        i += ix;
      }

      istart++;
    }

    ju /= 2;
    iy = ix;
    ix += ix;
    iheight -= iy;
  }

  for (iy = 0; iy < 16384; iy++) {
    y[iy].re *= 6.103515625E-5;
    y[iy].im *= 6.103515625E-5;
  }
}

/*
 * Arguments    : double x[6400]
 * Return Type  : void
 */
static void b_sign(double x[6400])
{
  int k;
  double b_x;
  for (k = 0; k < 6400; k++) {
    b_x = x[k];
    if (x[k] < 0.0) {
      b_x = -1.0;
    } else if (x[k] > 0.0) {
      b_x = 1.0;
    } else {
      if (x[k] == 0.0) {
        b_x = 0.0;
      }
    }

    x[k] = b_x;
  }
}

/*
 * Arguments    : double *x
 * Return Type  : void
 */
static void b_sqrt(double *x)
{
  *x = sqrt(*x);
}

/*
 * Arguments    : creal_T wwc[12799]
 * Return Type  : void
 */
static void bluesteinSetup(creal_T wwc[12799])
{
  int idx;
  int rt;
  int k;
  int y;
  double nt_im;
  double nt_re;
  idx = 6398;
  rt = 0;
  wwc[6399].re = 1.0;
  wwc[6399].im = 0.0;
  for (k = 0; k < 6399; k++) {
    y = ((1 + k) << 1) - 1;
    if (12800 - rt <= y) {
      rt = (y + rt) - 12800;
    } else {
      rt += y;
    }

    nt_im = 3.1415926535897931 * (double)rt / 6400.0;
    if (nt_im == 0.0) {
      nt_re = 1.0;
      nt_im = 0.0;
    } else {
      nt_re = cos(nt_im);
      nt_im = sin(nt_im);
    }

    wwc[idx].re = nt_re;
    wwc[idx].im = -nt_im;
    idx--;
  }

  idx = 0;
  for (k = 6398; k >= 0; k--) {
    wwc[k + 6400] = wwc[idx];
    idx++;
  }
}

/*
 * Arguments    : creal_T wwc[12799]
 * Return Type  : void
 */
static void bluestein_setup_impl(creal_T wwc[12799])
{
  int idx;
  int rt;
  int k;
  int y;
  double nt_im;
  double nt_re;
  idx = 6398;
  rt = 0;
  wwc[6399].re = 1.0;
  wwc[6399].im = 0.0;
  for (k = 0; k < 6399; k++) {
    y = ((1 + k) << 1) - 1;
    if (12800 - rt <= y) {
      rt = (y + rt) - 12800;
    } else {
      rt += y;
    }

    nt_im = -3.1415926535897931 * (double)rt / 6400.0;
    if (nt_im == 0.0) {
      nt_re = 1.0;
      nt_im = 0.0;
    } else {
      nt_re = cos(nt_im);
      nt_im = sin(nt_im);
    }

    wwc[idx].re = nt_re;
    wwc[idx].im = -nt_im;
    idx--;
  }

  idx = 0;
  for (k = 6398; k >= 0; k--) {
    wwc[k + 6400] = wwc[idx];
    idx++;
  }
}

/*
 * Arguments    : double x[5]
 * Return Type  : void
 */
static void c_sqrt(double x[5])
{
  int k;
  for (k = 0; k < 5; k++) {
    x[k] = sqrt(x[k]);
  }
}

/*
 * Arguments    : const double x[10]
 *                creal_T y[6400]
 * Return Type  : void
 */
static void fft(const double x[10], creal_T y[6400])
{
  static double costab[8193];
  static double sintab[8193];
  double sintabinv[8193];
  static creal_T wwc[12799];
  int xidx;
  int k;
  static creal_T fv[16384];
  static creal_T b_fv[16384];
  double fv_re;
  generate_twiddle_tables(costab, sintab, sintabinv);
  bluestein_setup_impl(wwc);
  for (xidx = 0; xidx < 6400; xidx++) {
    y[xidx].re = 0.0;
    y[xidx].im = 0.0;
  }

  xidx = 0;
  for (k = 0; k < 10; k++) {
    y[k].re = wwc[k + 6399].re * x[xidx];
    y[k].im = wwc[k + 6399].im * -x[xidx];
    xidx++;
  }

  for (k = 0; k < 6390; k++) {
    y[k + 10].re = 0.0;
    y[k + 10].im = 0.0;
  }

  r2br_r2dit_trig_impl(y, 0, costab, sintab, fv);
  r2br_r2dit_trig(wwc, costab, sintab, b_fv);
  for (xidx = 0; xidx < 16384; xidx++) {
    fv_re = b_fv[xidx].re;
    b_fv[xidx].re = fv[xidx].re * b_fv[xidx].re - fv[xidx].im * b_fv[xidx].im;
    b_fv[xidx].im = fv[xidx].re * b_fv[xidx].im + fv[xidx].im * fv_re;
  }

  b_r2br_r2dit_trig(b_fv, costab, sintabinv, fv);
  xidx = 0;
  for (k = 0; k < 6400; k++) {
    y[xidx].re = wwc[k + 6399].re * fv[k + 6399].re + wwc[k + 6399].im * fv[k +
      6399].im;
    y[xidx].im = wwc[k + 6399].re * fv[k + 6399].im - wwc[k + 6399].im * fv[k +
      6399].re;
    xidx++;
  }
}

/*
 * Arguments    : double costab[8193]
 *                double sintab[8193]
 *                double sintabinv[8193]
 * Return Type  : void
 */
static void generate_twiddle_tables(double costab[8193], double sintab[8193],
  double sintabinv[8193])
{
  double costab1q[4097];
  int k;
  double costab_tmp;
  double sintab_tmp;
  costab1q[0] = 1.0;
  for (k = 0; k < 2048; k++) {
    costab1q[1 + k] = cos(0.00038349519697141029 * (1.0 + (double)k));
  }

  for (k = 0; k < 2047; k++) {
    costab1q[k + 2049] = sin(0.00038349519697141029 * (4096.0 - ((double)k +
      2049.0)));
  }

  costab1q[4096] = 0.0;
  costab[0] = 1.0;
  sintab[0] = 0.0;
  for (k = 0; k < 4096; k++) {
    sintabinv[1 + k] = costab1q[4095 - k];
    sintabinv[k + 4097] = costab1q[k + 1];
  }

  for (k = 0; k < 4096; k++) {
    costab_tmp = costab1q[1 + k];
    costab[1 + k] = costab_tmp;
    sintab_tmp = -costab1q[4095 - k];
    sintab[1 + k] = sintab_tmp;
    costab[k + 4097] = sintab_tmp;
    sintab[k + 4097] = -costab_tmp;
  }
}

/*
 * Arguments    : const creal_T x[6400]
 *                creal_T y[6400]
 * Return Type  : void
 */
static void ifft(const creal_T x[6400], creal_T y[6400])
{
  static double costab[8193];
  static double sintab[8193];
  static double sintabinv[8193];
  static creal_T wwc[12799];
  int xidx;
  int k;
  static creal_T fv[16384];
  static creal_T b_fv[16384];
  double fv_re;
  double d1;
  generate_twiddle_tables(costab, sintab, sintabinv);
  bluesteinSetup(wwc);
  xidx = 0;
  for (k = 0; k < 6400; k++) {
    y[k].re = wwc[6399 + k].re * x[xidx].re + wwc[6399 + k].im * x[xidx].im;
    y[k].im = wwc[6399 + k].re * x[xidx].im - wwc[6399 + k].im * x[xidx].re;
    xidx++;
  }

  r2br_r2dit_trig_impl(y, 0, costab, sintab, fv);
  r2br_r2dit_trig(wwc, costab, sintab, b_fv);
  for (xidx = 0; xidx < 16384; xidx++) {
    fv_re = b_fv[xidx].re;
    b_fv[xidx].re = fv[xidx].re * b_fv[xidx].re - fv[xidx].im * b_fv[xidx].im;
    b_fv[xidx].im = fv[xidx].re * b_fv[xidx].im + fv[xidx].im * fv_re;
  }

  b_r2br_r2dit_trig(b_fv, costab, sintabinv, fv);
  xidx = 0;
  for (k = 0; k < 6400; k++) {
    fv_re = wwc[k + 6399].re * fv[k + 6399].re + wwc[k + 6399].im * fv[k + 6399]
      .im;
    y[xidx].re = fv_re;
    d1 = wwc[k + 6399].re * fv[k + 6399].im - wwc[k + 6399].im * fv[k + 6399].re;
    y[xidx].im = d1;
    y[xidx].re = fv_re;
    y[xidx].im = d1;
    if (y[xidx].im == 0.0) {
      y[xidx].re /= 6400.0;
      y[xidx].im = 0.0;
    } else if (y[xidx].re == 0.0) {
      y[xidx].re = 0.0;
      y[xidx].im /= 6400.0;
    } else {
      y[xidx].re /= 6400.0;
      y[xidx].im /= 6400.0;
    }

    xidx++;
  }
}

/*
 * Get the original input size
 *  Get the level of the MODWT
 * Arguments    : const double w[38400]
 *                creal_T xrec[6400]
 * Return Type  : void
 */
static void imodwt5(const double w[38400], creal_T xrec[6400])
{
  static creal_T G[6400];
  static creal_T H[6400];
  int i3;
  static double vin[6400];
  int jj;
  double b_w[6400];

  /*  Obtain reconstruction filters */
  /*  Scale scaling and wavelet filters for MODWT */
  /*  If the number of samples is less than the length of the scaling filter */
  /*  we have to periodize the data and then truncate. */
  /*  Get the DFTs of the scaling and wavelet filters */
  fft(dv0, G);
  fft(dv1, H);

  /*  Error */
  /*      if (lev>J) */
  /*          error(message('Wavelet:modwt:Incorrect_ReconLevel')); */
  /*      end */
  for (i3 = 0; i3 < 6400; i3++) {
    vin[i3] = w[5 + 6 * i3];
  }

  /*  IMODWT algorithm */
  for (jj = 0; jj < 5; jj++) {
    for (i3 = 0; i3 < 6400; i3++) {
      b_w[i3] = w[(6 * i3 - jj) + 4];
    }

    imodwtrec(vin, b_w, G, H, 5.0 + -(double)jj, xrec);
    for (i3 = 0; i3 < 6400; i3++) {
      vin[i3] = xrec[i3].re;
    }
  }

  /*  Return proper output length */
}

/*
 * Arguments    : const double Vin[6400]
 *                const double Win[6400]
 *                const creal_T G[6400]
 *                const creal_T H[6400]
 *                double J
 *                creal_T Vout[6400]
 * Return Type  : void
 */
static void imodwtrec(const double Vin[6400], const double Win[6400], const
                      creal_T G[6400], const creal_T H[6400], double J, creal_T
                      Vout[6400])
{
  double upfactor;
  int i4;
  double b_upfactor[6400];
  double dv6[6400];
  static creal_T Gup[6400];
  static creal_T dcv0[6400];
  static creal_T dcv1[6400];
  double H_re;
  double H_im;
  upfactor = mpower(2.0, J - 1.0);
  for (i4 = 0; i4 < 6400; i4++) {
    b_upfactor[i4] = upfactor * (double)i4;
  }

  b_mod(b_upfactor, dv6);
  for (i4 = 0; i4 < 6400; i4++) {
    Gup[i4].re = G[(int)(1.0 + dv6[i4]) - 1].re;
    Gup[i4].im = -G[(int)(1.0 + dv6[i4]) - 1].im;
  }

  for (i4 = 0; i4 < 6400; i4++) {
    b_upfactor[i4] = upfactor * (double)i4;
  }

  b_mod(b_upfactor, dv6);
  b_fft(Vin, dcv0);
  b_fft(Win, dcv1);
  for (i4 = 0; i4 < 6400; i4++) {
    upfactor = Gup[i4].re * dcv0[i4].im + Gup[i4].im * dcv0[i4].re;
    H_re = H[(int)(1.0 + dv6[i4]) - 1].re;
    H_im = -H[(int)(1.0 + dv6[i4]) - 1].im;
    Gup[i4].re = (Gup[i4].re * dcv0[i4].re - Gup[i4].im * dcv0[i4].im) + (H_re *
      dcv1[i4].re - H_im * dcv1[i4].im);
    Gup[i4].im = upfactor + (H_re * dcv1[i4].im + H_im * dcv1[i4].re);
  }

  ifft(Gup, Vout);
}

/*
 * Arguments    : double v[6400]
 *                int nv
 *                int ia
 *                int ib
 * Return Type  : void
 */
static void med3(double v[6400], int nv, int ia, int ib)
{
  int ic;
  double tmp;
  if (nv >= 3) {
    ic = ia + (nv - 1) / 2;
    if (v[ia - 1] < v[ic - 1]) {
      if (v[ic - 1] < v[ib - 1]) {
      } else if (v[ia - 1] < v[ib - 1]) {
        ic = ib;
      } else {
        ic = ia;
      }
    } else if (v[ia - 1] < v[ib - 1]) {
      ic = ia;
    } else {
      if (v[ic - 1] < v[ib - 1]) {
        ic = ib;
      }
    }

    if (ic > ia) {
      tmp = v[ia - 1];
      v[ia - 1] = v[ic - 1];
      v[ic - 1] = tmp;
    }
  }
}

/*
 * Arguments    : const double x[6400]
 * Return Type  : double
 */
static double median(const double x[6400])
{
  double y;
  int k;
  int exitg1;
  double v[6400];
  int ilast;
  double b;
  int unusedU2;
  k = 0;
  do {
    exitg1 = 0;
    if (k < 6400) {
      if (rtIsNaN(x[k])) {
        y = rtNaN;
        exitg1 = 1;
      } else {
        k++;
      }
    } else {
      memcpy(&v[0], &x[0], 6400U * sizeof(double));
      quickselect(v, 3201, 1, 6400, &y, &k, &ilast);
      if (3200 < k) {
        quickselect(v, 3200, 1, ilast - 1, &b, &k, &unusedU2);
        if (rtIsInf(y) || rtIsInf(b)) {
          y = (y + b) / 2.0;
        } else {
          y += (b - y) / 2.0;
        }
      }

      exitg1 = 1;
    }
  } while (exitg1 == 0);

  return y;
}

/*
 * Arguments    : double v[6400]
 *                int nv
 *                int ia
 * Return Type  : void
 */
static void medmed(double v[6400], int nv, int ia)
{
  int ngroupsof5;
  int nlast;
  int k;
  int i1;
  int destidx;
  double tmp;
  while (nv > 1) {
    ngroupsof5 = nv / 5;
    nlast = nv - ngroupsof5 * 5;
    nv = ngroupsof5;
    for (k = 0; k < ngroupsof5; k++) {
      i1 = ia + k * 5;
      i1 = thirdOfFive(v, i1, i1 + 4) - 1;
      destidx = (ia + k) - 1;
      tmp = v[destidx];
      v[destidx] = v[i1];
      v[i1] = tmp;
    }

    if (nlast > 0) {
      i1 = ia + ngroupsof5 * 5;
      i1 = thirdOfFive(v, i1, (i1 + nlast) - 1) - 1;
      destidx = (ia + ngroupsof5) - 1;
      tmp = v[destidx];
      v[destidx] = v[i1];
      v[i1] = tmp;
      nv = ngroupsof5 + 1;
    }
  }
}

/*
 * Convert data to row vector
 * Arguments    : const double x[6400]
 *                double w[38400]
 * Return Type  : void
 */
static void modwt5(const double x[6400], double w[38400])
{
  static creal_T G[6400];
  static creal_T H[6400];
  static creal_T Vhat[6400];
  int jj;
  static creal_T b_Vhat[6400];
  int i1;
  static creal_T What[6400];

  /*  Record original data length */
  /* Check that the level of the transform does not exceed floor(log2(numel(x)) */
  /*      J = 3; */
  /*      Jmax = floor(log2(siglen)); */
  /*      if (J <= 0) || (J > Jmax) || (J ~= fix(J)) */
  /*          error(message('Wavelet:modwt:MRALevel')); */
  /*      end */
  /*  Obtain Lo and Hi reconstruction filters */
  /*  Scale the scaling and wavelet filters for the MODWT */
  /*  Ensure Lo and Hi are row vectors */
  /*  If the signal length is less than the filter length, need to  */
  /*  periodize the signal in order to use the DFT algorithm */
  /*  Allocate coefficient array */
  /*  Obtain the DFT of the filters */
  fft(dv0, G);
  fft(dv1, H);

  /* Obtain the DFT of the data */
  b_fft(x, Vhat);

  /*  Main MODWT algorithm */
  for (jj = 0; jj < 5; jj++) {
    memcpy(&b_Vhat[0], &Vhat[0], 6400U * sizeof(creal_T));
    modwtdec(b_Vhat, G, H, 1.0 + (double)jj, Vhat, What);
    ifft(What, b_Vhat);
    for (i1 = 0; i1 < 6400; i1++) {
      w[jj + 6 * i1] = b_Vhat[i1].re;
    }
  }

  ifft(Vhat, b_Vhat);
  for (i1 = 0; i1 < 6400; i1++) {
    w[5 + 6 * i1] = b_Vhat[i1].re;
  }

  /*  Truncate data to length of boundary condition */
}

/*
 * [Vhat,What] = modwtfft(X,G,H,J)
 * Arguments    : const creal_T X[6400]
 *                const creal_T G[6400]
 *                const creal_T H[6400]
 *                double J
 *                creal_T Vhat[6400]
 *                creal_T What[6400]
 * Return Type  : void
 */
static void modwtdec(const creal_T X[6400], const creal_T G[6400], const creal_T
                     H[6400], double J, creal_T Vhat[6400], creal_T What[6400])
{
  double upfactor;
  int i2;
  double b_upfactor[6400];
  double dv4[6400];
  upfactor = mpower(2.0, J - 1.0);
  for (i2 = 0; i2 < 6400; i2++) {
    b_upfactor[i2] = upfactor * (double)i2;
  }

  b_mod(b_upfactor, dv4);
  for (i2 = 0; i2 < 6400; i2++) {
    Vhat[i2] = G[(int)(1.0 + dv4[i2]) - 1];
  }

  for (i2 = 0; i2 < 6400; i2++) {
    b_upfactor[i2] = upfactor * (double)i2;
  }

  b_mod(b_upfactor, dv4);
  for (i2 = 0; i2 < 6400; i2++) {
    upfactor = Vhat[i2].re;
    Vhat[i2].re = Vhat[i2].re * X[i2].re - Vhat[i2].im * X[i2].im;
    Vhat[i2].im = upfactor * X[i2].im + Vhat[i2].im * X[i2].re;
    What[i2].re = H[(int)(1.0 + dv4[i2]) - 1].re * X[i2].re - H[(int)(1.0 +
      dv4[i2]) - 1].im * X[i2].im;
    What[i2].im = H[(int)(1.0 + dv4[i2]) - 1].re * X[i2].im + H[(int)(1.0 +
      dv4[i2]) - 1].im * X[i2].re;
  }
}

/*
 * Arguments    : double a
 *                double b
 * Return Type  : double
 */
static double mpower(double a, double b)
{
  return rt_powd_snf(a, b);
}

/*
 * Arguments    : double v[6400]
 *                int *ip
 *                int ia
 *                int ib
 * Return Type  : int
 */
static int pivot(double v[6400], int *ip, int ia, int ib)
{
  int reps;
  double vref;
  int i5;
  int k;
  double vk;
  double d4;
  vref = v[*ip - 1];
  v[*ip - 1] = v[ib - 1];
  v[ib - 1] = vref;
  *ip = ia;
  reps = 0;
  i5 = ib - 1;
  for (k = ia; k <= i5; k++) {
    vk = v[k - 1];
    d4 = v[k - 1];
    if (d4 == vref) {
      v[k - 1] = v[*ip - 1];
      v[*ip - 1] = vk;
      reps++;
      (*ip)++;
    } else {
      if (d4 < vref) {
        v[k - 1] = v[*ip - 1];
        v[*ip - 1] = vk;
        (*ip)++;
      }
    }
  }

  v[ib - 1] = v[*ip - 1];
  v[*ip - 1] = vref;
  return reps;
}

/*
 * Arguments    : const double a[5]
 *                double y[5]
 * Return Type  : void
 */
static void power(const double a[5], double y[5])
{
  int k;
  for (k = 0; k < 5; k++) {
    y[k] = rt_powd_snf(a[k], 2.0);
  }
}

/*
 * Arguments    : double v[6400]
 *                int n
 *                int idx0
 *                int vlen
 *                double *vn
 *                int *nfirst
 *                int *nlast
 * Return Type  : void
 */
static void quickselect(double v[6400], int n, int idx0, int vlen, double *vn,
  int *nfirst, int *nlast)
{
  int itarget;
  int ipiv;
  int ia;
  int ib;
  int ifirst;
  int ilast;
  int oldnv;
  bool checkspeed;
  bool isslow;
  bool exitg1;
  bool guard1 = false;
  if ((n < 1) || (n > vlen)) {
    *vn = rtNaN;
    *nfirst = 0;
    *nlast = 0;
  } else {
    itarget = (idx0 + n) - 1;
    ipiv = itarget;
    ia = idx0;
    ib = (idx0 + vlen) - 1;
    ifirst = idx0;
    ilast = ib;
    oldnv = vlen;
    checkspeed = false;
    isslow = false;
    exitg1 = false;
    while ((!exitg1) && (ia < ib)) {
      ifirst = pivot(v, &ipiv, ia, ib);
      ilast = ipiv;
      guard1 = false;
      if (itarget <= ipiv) {
        ifirst = ipiv - ifirst;
        if (itarget >= ifirst) {
          exitg1 = true;
        } else {
          ib = ipiv - 1;
          guard1 = true;
        }
      } else {
        ia = ipiv + 1;
        guard1 = true;
      }

      if (guard1) {
        ifirst = (ib - ia) + 1;
        if (checkspeed) {
          isslow = (ifirst > oldnv / 2);
          oldnv = ifirst;
        }

        checkspeed = !checkspeed;
        if (isslow) {
          medmed(v, ifirst, ia);
        } else {
          med3(v, ifirst, ia, ib);
        }

        ipiv = ia;
        ifirst = ia;
        ilast = ib;
      }
    }

    *vn = v[ilast - 1];
    *nfirst = (ifirst - idx0) + 1;
    *nlast = (ilast - idx0) + 1;
  }
}

/*
 * Arguments    : const creal_T x[12799]
 *                const double costab[8193]
 *                const double sintab[8193]
 *                creal_T y[16384]
 * Return Type  : void
 */
static void r2br_r2dit_trig(const creal_T x[12799], const double costab[8193],
  const double sintab[8193], creal_T y[16384])
{
  int i;
  int ix;
  int ju;
  int iy;
  bool tst;
  double temp_re;
  double temp_im;
  double twid_re;
  int iheight;
  double twid_im;
  int istart;
  int temp_re_tmp;
  int j;
  int ihi;
  for (i = 0; i < 16384; i++) {
    y[i].re = 0.0;
    y[i].im = 0.0;
  }

  ix = 0;
  ju = 0;
  iy = 0;
  for (i = 0; i < 12798; i++) {
    y[iy] = x[ix];
    iy = 16384;
    tst = true;
    while (tst) {
      iy >>= 1;
      ju ^= iy;
      tst = ((ju & iy) == 0);
    }

    iy = ju;
    ix++;
  }

  y[iy] = x[ix];
  for (i = 0; i <= 16382; i += 2) {
    temp_re = y[i + 1].re;
    temp_im = y[i + 1].im;
    twid_re = y[i].re;
    twid_im = y[i].im;
    y[i + 1].re = y[i].re - y[i + 1].re;
    y[i + 1].im = y[i].im - y[i + 1].im;
    twid_re += temp_re;
    twid_im += temp_im;
    y[i].re = twid_re;
    y[i].im = twid_im;
  }

  iy = 2;
  ix = 4;
  ju = 4096;
  iheight = 16381;
  while (ju > 0) {
    for (i = 0; i < iheight; i += ix) {
      temp_re_tmp = i + iy;
      temp_re = y[temp_re_tmp].re;
      temp_im = y[i + iy].im;
      y[temp_re_tmp].re = y[i].re - y[temp_re_tmp].re;
      y[temp_re_tmp].im = y[i].im - temp_im;
      y[i].re += temp_re;
      y[i].im += temp_im;
    }

    istart = 1;
    for (j = ju; j < 8192; j += ju) {
      twid_re = costab[j];
      twid_im = sintab[j];
      i = istart;
      ihi = istart + iheight;
      while (i < ihi) {
        temp_re_tmp = i + iy;
        temp_re = twid_re * y[temp_re_tmp].re - twid_im * y[i + iy].im;
        temp_im = twid_re * y[i + iy].im + twid_im * y[i + iy].re;
        y[temp_re_tmp].re = y[i].re - temp_re;
        y[temp_re_tmp].im = y[i].im - temp_im;
        y[i].re += temp_re;
        y[i].im += temp_im;
        i += ix;
      }

      istart++;
    }

    ju /= 2;
    iy = ix;
    ix += ix;
    iheight -= iy;
  }
}

/*
 * Arguments    : const creal_T x[6400]
 *                int xoffInit
 *                const double costab[8193]
 *                const double sintab[8193]
 *                creal_T y[16384]
 * Return Type  : void
 */
static void r2br_r2dit_trig_impl(const creal_T x[6400], int xoffInit, const
  double costab[8193], const double sintab[8193], creal_T y[16384])
{
  int i;
  int ix;
  int ju;
  int iy;
  bool tst;
  double temp_re;
  double temp_im;
  double twid_re;
  int iheight;
  double twid_im;
  int istart;
  int temp_re_tmp;
  int j;
  int ihi;
  for (i = 0; i < 16384; i++) {
    y[i].re = 0.0;
    y[i].im = 0.0;
  }

  ix = xoffInit;
  ju = 0;
  iy = 0;
  for (i = 0; i < 6399; i++) {
    y[iy] = x[ix];
    iy = 16384;
    tst = true;
    while (tst) {
      iy >>= 1;
      ju ^= iy;
      tst = ((ju & iy) == 0);
    }

    iy = ju;
    ix++;
  }

  y[iy] = x[ix];
  for (i = 0; i <= 16382; i += 2) {
    temp_re = y[i + 1].re;
    temp_im = y[i + 1].im;
    twid_re = y[i].re;
    twid_im = y[i].im;
    y[i + 1].re = y[i].re - y[i + 1].re;
    y[i + 1].im = y[i].im - y[i + 1].im;
    twid_re += temp_re;
    twid_im += temp_im;
    y[i].re = twid_re;
    y[i].im = twid_im;
  }

  iy = 2;
  ix = 4;
  ju = 4096;
  iheight = 16381;
  while (ju > 0) {
    for (i = 0; i < iheight; i += ix) {
      temp_re_tmp = i + iy;
      temp_re = y[temp_re_tmp].re;
      temp_im = y[i + iy].im;
      y[temp_re_tmp].re = y[i].re - y[temp_re_tmp].re;
      y[temp_re_tmp].im = y[i].im - temp_im;
      y[i].re += temp_re;
      y[i].im += temp_im;
    }

    istart = 1;
    for (j = ju; j < 8192; j += ju) {
      twid_re = costab[j];
      twid_im = sintab[j];
      i = istart;
      ihi = istart + iheight;
      while (i < ihi) {
        temp_re_tmp = i + iy;
        temp_re = twid_re * y[temp_re_tmp].re - twid_im * y[i + iy].im;
        temp_im = twid_re * y[i + iy].im + twid_im * y[i + iy].re;
        y[temp_re_tmp].re = y[i].re - temp_re;
        y[temp_re_tmp].im = y[i].im - temp_im;
        y[i].re += temp_re;
        y[i].im += temp_im;
        i += ix;
      }

      istart++;
    }

    ju /= 2;
    iy = ix;
    ix += ix;
    iheight -= iy;
  }
}

/*
 * Arguments    : double u0
 *                double u1
 * Return Type  : double
 */
static double rt_powd_snf(double u0, double u1)
{
  double y;
  double d2;
  double d3;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = rtNaN;
  } else {
    d2 = fabs(u0);
    d3 = fabs(u1);
    if (rtIsInf(u1)) {
      if (d2 == 1.0) {
        y = 1.0;
      } else if (d2 > 1.0) {
        if (u1 > 0.0) {
          y = rtInf;
        } else {
          y = 0.0;
        }
      } else if (u1 > 0.0) {
        y = 0.0;
      } else {
        y = rtInf;
      }
    } else if (d3 == 0.0) {
      y = 1.0;
    } else if (d3 == 1.0) {
      if (u1 > 0.0) {
        y = u0;
      } else {
        y = 1.0 / u0;
      }
    } else if (u1 == 2.0) {
      y = u0 * u0;
    } else if ((u1 == 0.5) && (u0 >= 0.0)) {
      y = sqrt(u0);
    } else if ((u0 < 0.0) && (u1 > floor(u1))) {
      y = rtNaN;
    } else {
      y = pow(u0, u1);
    }
  }

  return y;
}

/*
 * Arguments    : const double v[6400]
 *                int ia
 *                int ib
 * Return Type  : int
 */
static int thirdOfFive(const double v[6400], int ia, int ib)
{
  int im;
  double v4;
  double v5_tmp;
  int b_j1;
  int j2;
  int j3;
  int j4;
  int j5;
  double v5;
  if ((ia == ib) || (ia + 1 == ib)) {
    im = ia;
  } else if ((ia + 2 == ib) || (ia + 3 == ib)) {
    if (v[ia - 1] < v[ia]) {
      if (v[ia] < v[ia + 1]) {
        im = ia + 1;
      } else if (v[ia - 1] < v[ia + 1]) {
        im = ia + 2;
      } else {
        im = ia;
      }
    } else if (v[ia - 1] < v[ia + 1]) {
      im = ia;
    } else if (v[ia] < v[ia + 1]) {
      im = ia + 2;
    } else {
      im = ia + 1;
    }
  } else {
    v4 = v[ia - 1];
    if (v4 < v[ia]) {
      if (v[ia] < v[ia + 1]) {
        b_j1 = ia;
        j2 = ia;
        j3 = ia + 2;
      } else if (v4 < v[ia + 1]) {
        b_j1 = ia;
        j2 = ia + 1;
        j3 = ia + 1;
      } else {
        b_j1 = ia + 2;
        j2 = ia - 1;
        j3 = ia + 1;
      }
    } else {
      v5_tmp = v[ia + 1];
      if (v4 < v5_tmp) {
        b_j1 = ia + 1;
        j2 = ia - 1;
        j3 = ia + 2;
      } else if (v[ia] < v5_tmp) {
        b_j1 = ia + 1;
        j2 = ia + 1;
        j3 = ia;
      } else {
        b_j1 = ia + 2;
        j2 = ia;
        j3 = ia;
      }
    }

    j4 = ia;
    j5 = ia + 1;
    v4 = v[ia + 2];
    v5_tmp = v[ia + 3];
    v5 = v5_tmp;
    if (v5_tmp < v4) {
      j4 = ia + 1;
      j5 = ia;
      v5 = v4;
      v4 = v5_tmp;
    }

    if (v5 < v[b_j1 - 1]) {
      im = b_j1;
    } else if (v5 < v[j2]) {
      im = j5 + 3;
    } else if (v4 < v[j2]) {
      im = j2 + 1;
    } else if (v4 < v[j3 - 1]) {
      im = j4 + 3;
    } else {
      im = j3;
    }
  }

  return im;
}

/*
 * WTHRESH Perform soft or hard thresholding.
 *    Y = WTHRESH(X,SORH,T) returns soft (if SORH = 's')
 *    or hard (if SORH = 'h') T-thresholding  of the input
 *    vector or matrix X. T is the threshold value.
 *
 *    Y = WTHRESH(X,'s',T) returns Y = SIGN(X).(|X|-T)+, soft
 *    thresholding is shrinkage.
 *
 *    Y = WTHRESH(X,'h',T) returns Y = X.1_(|X|>T), hard
 *    thresholding is cruder.
 * Arguments    : const double x[6400]
 *                char sorh
 *                double t
 *                double y[6400]
 * Return Type  : void
 */
static void wthresh(const double x[6400], char sorh, double t, double y[6400])
{
  bool b_bool;
  int b_index;
  double tmp[6400];
  double dv5[6400];
  b_bool = false;
  if (!(sorh != 's')) {
    b_bool = true;
  }

  if (b_bool) {
    b_index = 0;
  } else {
    b_bool = false;
    if (!(sorh != 'h')) {
      b_bool = true;
    }

    if (b_bool) {
      b_index = 1;
    } else {
      b_index = -1;
    }
  }

  switch (b_index) {
   case 0:
    b_abs(x, tmp);
    for (b_index = 0; b_index < 6400; b_index++) {
      tmp[b_index] -= t;
    }

    b_abs(tmp, dv5);
    for (b_index = 0; b_index < 6400; b_index++) {
      tmp[b_index] = (tmp[b_index] + dv5[b_index]) / 2.0;
      y[b_index] = x[b_index];
    }

    b_sign(y);
    for (b_index = 0; b_index < 6400; b_index++) {
      y[b_index] *= tmp[b_index];
    }
    break;

   case 1:
    b_abs(x, y);
    for (b_index = 0; b_index < 6400; b_index++) {
      y[b_index] = x[b_index] * (double)(y[b_index] > t);
    }
    break;

   default:
    b_abs(x, y);
    for (b_index = 0; b_index < 6400; b_index++) {
      y[b_index] = x[b_index] * (double)(y[b_index] > t);
    }
    break;
  }
}

/*
 * Arguments    : const double ecgdat_r[6400]
 *                char sh
 *                creal_T ecgdat_dn[6400]
 *                double cxd[38400]
 *                double thr[5]
 * Return Type  : void
 */
void db5lev5(const double ecgdat_r[6400], char sh, creal_T ecgdat_dn[6400],
             double cxd[38400], double thr[5])
{
  int kk;
  double d0;
  double madest[5];
  int i0;
  static const signed char iv0[5] = { 2, 4, 8, 16, 32 };

  static double b_cxd[6400];
  static double dv2[6400];
  static double dv3[6400];

  /*    [xd,cxd,thr] = modwtdenoise1D(x,wav,lev,softHard,scal) returns denoised */
  /*    1D signal xd and MODWT coefficients cxd.    */
  /*  Obtain the MODWT */
  modwt5(ecgdat_r, cxd);

  /*  Determine the level dependent thresholds */
  for (kk = 0; kk < 5; kk++) {
    d0 = 2.0;
    b_sqrt(&d0);
    for (i0 = 0; i0 < 6400; i0++) {
      b_cxd[i0] = cxd[kk + 6 * i0];
    }

    b_abs(b_cxd, dv2);
    madest[kk] = d0 * median(dv2) / 0.6745;
  }

  /*  Thresholds */
  power(madest, thr);
  for (i0 = 0; i0 < 5; i0++) {
    thr[i0] = 2.0 * thr[i0] / (double)iv0[i0] * 8.7640532693477624;
  }

  c_sqrt(thr);

  /*  Threshold MODWT coefficients */
  for (kk = 0; kk < 5; kk++) {
    for (i0 = 0; i0 < 6400; i0++) {
      b_cxd[i0] = cxd[kk + 6 * i0];
    }

    wthresh(b_cxd, sh, thr[kk], dv3);
    for (i0 = 0; i0 < 6400; i0++) {
      cxd[kk + 6 * i0] = dv3[i0];
    }
  }

  /*  Invert the MODWT     */
  imodwt5(cxd, ecgdat_dn);

  /*      [ecgdat_dn, cxd, thr] = modwtdenoise1D(ecgdat, 'db5', 'h'); */
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void db5lev5_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

/*
 * Arguments    : void
 * Return Type  : void
 */
void db5lev5_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for db5lev5.c
 *
 * [EOF]
 */
