/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: db5lev5.h
 *
 * MATLAB Coder version            : 4.2
 * C/C++ source code generated on  : 28-Feb-2020 16:40:56
 */

#ifndef DB5LEV5_H
#define DB5LEV5_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "db5lev5_types.h"

/* Function Declarations */
extern void db5lev5(const double ecgdat_r[6400], char sh, creal_T ecgdat_dn[6400],double cxd[38400], double thr[5]);
extern void db5lev5_initialize(void);
extern void db5lev5_terminate(void);

#endif

/*
 * File trailer for db5lev5.h
 *
 * [EOF]
 */
