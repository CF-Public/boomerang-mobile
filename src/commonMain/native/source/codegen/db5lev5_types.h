/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: db5lev5_types.h
 *
 * MATLAB Coder version            : 4.2
 * C/C++ source code generated on  : 28-Feb-2020 16:40:56
 */

#ifndef DB5LEV5_TYPES_H
#define DB5LEV5_TYPES_H

/* Include Files */
#include "rtwtypes.h"
#endif

/*
 * File trailer for db5lev5_types.h
 *
 * [EOF]
 */
