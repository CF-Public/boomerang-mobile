#include "octave.hpp"

#include <vector>
#include <cstdint>
#include <exception>

#include <xtensor/xarray.hpp>
#include <xtensor/xsort.hpp>
#include <xtensor/xrandom.hpp>
#include <xtensor/xview.hpp>
#include <xtensor/xadapt.hpp>
#include <xtensor/xarray.hpp>
#include <kissfft/kiss_fft.h>
#include <kissfft/kiss_fft.h>
#include "util.hpp"


int next_pow2(double n) {
    return (int) ceil(log2(abs(n)));
}

double prctile(xt::xarray<double> const &array, int percentile) {
    if (array.dimension() > 1) {
        throw DSPKitException("Prctile only handles vectors");
    }
    if (array.shape(0) == 0) {
        throw DSPKitException("Empty array");
    }

    if (percentile > 100) {
        throw DSPKitException("Percentile cannot be greater than one");
    }
    if (percentile < 0) {
        throw DSPKitException("Percentile cannot be less than zero");
    }

    auto sorted = xt::sort(array);
    auto N = sorted.shape(0);
    for (int i = 1; i <= N; i++) {
        auto rank = (100.0 / (double) N) * (i - 0.5);

        auto returnCurrentRank =
                (i == 1 && percentile <= rank)    // If P <= P1 return first element
                || (i == N && percentile >= rank) // If P >= PN return last element
                || (percentile == rank);          // If P == Px return x element

        if (returnCurrentRank) {
            return sorted(i - 1);
        }

        // If rank is between rank(k) and rank(k + 1),
        // return a linear interpolation of the result
        auto nextRank = (100.0 / (double) N) * (i + 1 - 0.5);
        if (percentile > rank && percentile < nextRank && i < N) {
            return sorted(i - 1) +
                   ((percentile - rank) / (nextRank - rank)) * (sorted(i) - sorted(i - 1));
        }
    }
    return 0; // This should never happen though
}

xt::xarray<double> awgn(xt::xarray<double> const &array, double snrDB) {
    auto np = noisePower(array, snrDB);
    return array + sqrt(np) * xt::random::randn(array.shape(), 0.0, 1.0);
}

inline double noisePower(xt::xarray<double> const &array, double snrDB) {
    auto length = array.shape(0);
    // Normalized signal to power
    double p = (xt::sum(xt::pow(xt::abs(array), 2))()) / (double) length;
    double snr = pow(10, snrDB / (double) 10);

    // Power of noise, np
    return p / snr;
}

xt::xarray<std::complex<double>> rfft(xt::xarray<double> const &input) {
    auto nfft = input.shape(0);
    std::vector<double> output(nfft, 0.0);
    kiss_fft_cfg cfg = kiss_fft_alloc(nfft, 0, nullptr, nullptr);
    auto *cx_in = new kiss_fft_cpx[nfft];
    auto *cx_out = new kiss_fft_cpx[nfft];

    for (int i = 0; i < nfft; i++) {
        cx_in[i].r = input[i];
        cx_in[i].i = 0.0;
    }
    kiss_fft(cfg, cx_in, cx_out);
    for (int i = 0; i < nfft; i++) {
        output[i] = cx_out[i].r;
    }

    delete[] cx_in;
    delete[] cx_out;

    std::vector<std::size_t> shape = {nfft};
    return xt::adapt(output, shape);
}

void rfft(const int16_t *input, double *output, uint32_t size) {
    std::vector<std::size_t> shape = {size};
    auto array = xt::adapt(input, size, xt::no_ownership(), shape);
    auto output_array = rfft(array);
    for (int i = 0; i < size; i++) {
        output[i] = output_array[i].real();
    }
}


std::tuple<xt::xarray<double>, xt::xarray<double>>
periodogram(xt::xarray<double> const &data, int nfft, int range, double fs) {
    auto length = data.shape(0);

    // Validate arguments
    if (data.dimension() != 1) {
        throw DSPKitException("Input must be a vector");
    }
    if (!(range == ONE_SIDED || range == TWO_SIDED)) {
        throw DSPKitException("Invalid range argument");
    }
    if (nfft == -1) {
        nfft = std::max(256, (int) std::pow(2, next_pow2(length)));
    }

    xt::xarray<double> array;

    // Make sure length(array) == nfft
    if (length > nfft) {
        array = xt::view(data, xt::range(_, nfft));
    } else if (nfft > length) {
        array = xt::concatenate(xt::xtuple(data, xt::zeros<double>({nfft - length})), 0);
    } else {
        array = data;
    }

    // compute periodogram
    xt::xarray<double> Pxx;
    auto fft = rfft(array);
    Pxx = (xt::pow(xt::abs(fft), 2.0) / (double) length) / fs;

    // generate output arguments
    if (range == ONE_SIDED) // onesided
    {
        if (nfft % 2 == 0) // nfft is even
        {
            int psdLen = nfft / 2 + 1;
            auto tmp = Pxx;

            Pxx = xt::view(Pxx, xt::range(_, psdLen)) +
                  xt::concatenate(xt::xtuple(
                          xt::zeros<double>({1}),
                          xt::view(Pxx, xt::range(nfft - 1, psdLen - 1, -1)),
                          xt::zeros<double>({1})),
                                  0);
        } else // nfft is odd
        {
            int psdLen = (nfft + 1) / 2;
            Pxx = xt::view(Pxx, xt::range(_, psdLen)) +
                  xt::concatenate(xt::xtuple(
                          xt::zeros<double>({1}),
                          xt::view(Pxx, xt::range(nfft - 1, psdLen - 1, -1))),
                                  0);
        }
    }

    xt::xarray<double> f;
    if (range == ONE_SIDED) {
        // FIXME: The stop range
        f = xt::arange<double>(0, nfft / 2 + 1) / (double) (nfft);
    } else {
        f = xt::arange<double>(0, nfft) / (double) (nfft);
    }

    return std::make_tuple(Pxx, f);
}


double
bandPower(xt::xarray<double> const &array, double frequency, int lowerLimit, int upperLimit) {
    xt::xarray<double> p, f;
    tie(p, f) = periodogram(array);

    auto length = array.shape(0);
    int lower = (int) std::floor(lowerLimit * length / frequency);
    int upper = (int) std::floor(upperLimit * length / frequency);
    auto indices = xt::range(std::max(0, lower), upper);
    return 2 * xt::sum(xt::view(p, indices))() / length;
}

double stdDevSample(xt::xarray<double> const &array) {
    auto length = (double) array.shape(0);
    return sqrt(xt::sum(xt::pow(xt::abs(array), 2.0))() / length);
}