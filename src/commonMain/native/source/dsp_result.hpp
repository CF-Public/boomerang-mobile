#include <utility>
#include <vector>
#include <scapix/bridge/object.h>

class dsp_result : public scapix::bridge::object<dsp_result> {
private:
    std::vector<double> data;
    bool noisy;
public:
    dsp_result(std::vector<double> data, bool isNoisy) : data(std::move(data)), noisy(isNoisy) {}

    [[nodiscard]] const std::vector<double> &getData() const {
        return data;
    }

    [[nodiscard]] bool isNoisy() const {
        return noisy;
    }
};