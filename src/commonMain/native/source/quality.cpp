#include <vector>
#include <cmath>
#include <cstddef>

#include <xtensor/xadapt.hpp>
#include <xtensor/xarray.hpp>

#include "octave.hpp"
#include "quality.hpp"


static const int START_INDEX = 1023;
static const int END_INDEX = 6400;
static const int SIG_LEN = END_INDEX - START_INDEX;
static const int N = (int) std::pow(2, next_pow2(END_INDEX));

/*
 * Compute the exponent for the  smallest power of two larger than the input.
 */
static xt::xarray<double> nextpow2(xt::xarray<double> const &array) {
    return xt::ceil(xt::log2(xt::abs(array)));
}


static inline void wrapAround(xt::xarray<double> &array, int offset, int end) {
    int start = 0;
    while (offset < end) {
        array(offset++) = array(start++);
    }
}

/**
 *
 * @param signal a 1-D signal.
 */
bool isNoisy(xt::xarray<double> const &signal) {
    // STEP 1
    // Prepare data
    int sigLen = (int) signal.shape(0);
    xt::xarray<double> data; // a column-vector

    if (sigLen < END_INDEX) {
        xt::xarray<double>::shape_type shape = {(size_t) (SIG_LEN - sigLen)};
        data = xt::concatenate(xt::xtuple(
                signal,
                xt::zeros<double>(shape)),
                               0);
        wrapAround(data, sigLen, SIG_LEN);
    } else {
        data = signal;
    }
    data = xt::view(data, xt::range(START_INDEX, END_INDEX));

    // STEP 2
    // If ecg data > 99%tile or < 1%tile,
    // clip data to within 1-99%tile
    auto p99 = prctile(data, 99);
    xt::xarray<double> clipped = xt::where(data > p99 * 1.3, p99 * 1.3, data);

    auto p1 = prctile(clipped, 1);
    clipped = xt::where(clipped < p1 * 1.3, p1 * 1.3, clipped);

    // STEP 3
    // Calculate power percentages
    auto fs = 256;      // sampling frequency
    auto fnyq = fs / 2; // nyquist frequency

    auto pL40 = bandPower(clipped, fs, 0, 40);
    auto pL128 = bandPower(clipped, fs, 0, 127);
    auto signalPowerPct = 100.0 * (pL40 / pL128);

    // STEP 4
    // Calculate standard deviation
    auto stdev = stdDevSample(clipped);

    // STEP 5
    return signalPowerPct < 80.96 || stdev > 234.9;
}

bool isNoisy(const double *const signal, std::size_t size) {
    std::vector<std::size_t> shape = {size};
    auto array = xt::adapt(signal, size, xt::no_ownership(), shape);
    return isNoisy(array);
}


bool isNoisy(const std::vector<double> &signal) {
    std::vector<std::size_t> shape = {signal.size()};
    auto array = xt::adapt(signal, shape);
    return isNoisy(array);
}