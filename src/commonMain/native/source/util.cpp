#include "util.hpp"

#ifdef __ANDROID__

#include <android/log.h>

#endif

#include <string>
#include <cstdarg>
#include <cstdint>
#include <vector>


static void platform_log(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
#ifdef __ANDROID__
    __android_log_vprint(ANDROID_LOG_VERBOSE, "ndk", fmt, args);
#else
    vprintf(fmt, args);
#endif
    va_end(args);
}

void platform_log_vector(const std::vector<double> &arr, int length) {
    std::string input_str;
    for (int i = 0; i < length; i++) {
        input_str += std::to_string(arr[i]) + ", ";
    }
    platform_log("VECTOR: %s", input_str.c_str());
}

void platform_log_short_array(const int16_t *arr, int length) {
    std::string input_str;
    for (int i = 0; i < length; i++) {
        input_str += std::to_string(arr[i]) + ", ";
    }
    platform_log("INT ARR: %s", input_str.c_str());
}

void platform_log_double_array(double *arr, int length) {
    std::string input_str;
    for (int i = 0; i < length; i++) {
        input_str += std::to_string(arr[i]) + ", ";
    }
    platform_log("DOUBLE ARR: %s", input_str.c_str());
}


