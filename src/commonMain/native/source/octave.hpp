#ifndef octave_hpp
#define octave_hpp

#include <algorithm>
#include <tuple>
#define _USE_MATH_DEFINES
#include <math.h>
#include <cstdio>

#include <xtensor/xadapt.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xsort.hpp>
#include <xtensor/xrandom.hpp>
#include <xtensor/xview.hpp>


using namespace xt::placeholders; // required for `_` to work

const int ONE_SIDED = 1;
const int TWO_SIDED = 2;

/**
 * Returns the least "power of two" that is greater than
 * the input @param n.
 * For example:
 *      next_pow2(5) == 8
 *      next_pow2(20) == 32
 *
 * @param n
 * @return - power of two
 */
int next_pow2(double n);

/**
 * Matlab-like Percentile (of a 1-D array)
 *
 * Calculation lifted from https://www.mathworks.com/help/stats/prctile.html
 * @param array - One-Dimensional (1-D) array
 * @param percentile - percentile of interest
 *
 * @return - percentile
 */
double prctile(xt::xarray<double> const &array, int percentile);


inline double noisePower(xt::xarray<double> const &array, double snrDB);

/**
 * Add white noise to signal
 */
xt::xarray<double> awgn(xt::xarray<double> const &array, double snrDB);


/**
 * Returns full fourier transform spectrum of signal.
 *
 * @param input - 1-Dimensional signal
 *
 * @return fourier transform with sample length as the input.
 */
static xt::xarray<std::complex<double>> rfft(xt::xarray<double> const &input);

/**
 * Returns the periodogram (Power Spectral Density of X)
 *
 * @param data vector
 * @param nfft number of frequency bins.
 *          Defaults to 256 or the next higher power of 2 greater than the length of
 *          data (`max(256, 2^nextpower2(length(data))`).
 *          If nfft is greater than the length of the input then data will be zero-padded
 *          to the length of nfft.
 * @param range range of spectrum. Defaults to ONE_SIDED (real signals)
 * @param fs sampling rate. The default is 1
 */
std::tuple<xt::xarray<double>, xt::xarray<double>>
periodogram(xt::xarray<double> const &data, int nfft = -1, int range = ONE_SIDED,
            double fs = 2 * M_PI);

double
bandPower(xt::xarray<double> const &array, double frequency, int lowerLimit, int upperLimit);

/**
 * Calculates and returns the Standard Deviation of sample
 *
 * @param array - 1-Dimensional vector
 */
double stdDevSample(xt::xarray<double> const &array);

void rfft(const int16_t *input, double *output, uint32_t size);

#endif /* octave_hpp */
