cmake_minimum_required(VERSION 3.14)

project(dsp)

find_package(Java COMPONENTS Development)
include(UseJava)

scapix_fix_sources(dspkit)

file(GLOB_RECURSE sources CONFIGURE_DEPENDS "*.java")
get_target_property(generated_sources dspkit INTERFACE_SOURCES)
get_target_property(scapix_sources scapix INTERFACE_SOURCES)

add_jar(dsp
  SOURCES
    ${sources}
    ${generated_sources}
    ${scapix_sources}
  ENTRY_POINT
    com/comfithealth/boomerang/dsp
)

add_dependencies(dsp dspkit)

add_custom_target(run ALL
  COMMAND ${Java_JAVA_EXECUTABLE} -cp "${CMAKE_CURRENT_BINARY_DIR}" -Djava.library.path="$<TARGET_FILE_DIR:dspkit>" -jar dsp.jar
)

add_dependencies(run dsp)