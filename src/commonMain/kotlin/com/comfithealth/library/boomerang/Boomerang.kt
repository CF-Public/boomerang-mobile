/*
 * Copyright (c) 2021, Comfit Healthcare Devices Limited
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms will only be
 * allowed with prior written permission from authorized personnel
 * in Comfit Healthcare Devices Limited
 *
 * THIS SOFTWARE IS PROVIDED BY COMFIT HEALTHCARE DEVICES LIMITED "AS IS" AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COMFIT HEALTHCARE DEVICES LIMITED OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.comfithealth.library.boomerang

import com.comfithealth.library.models.BoomerangState
import com.comfithealth.library.models.BoomerangInfo
import com.comfithealth.library.models.HeartRateData
import com.comfithealth.library.models.Status
import com.comfithealth.library.common.BoomerangException
import com.github.michaelbull.result.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow

/***
 * Boomerang ECG device
 */
interface Boomerang {
    val address: String
    val ecg: Flow<ByteArray>
    val heartRate: Flow<HeartRateData>
    val status: SharedFlow<Status>
    val pace: Flow<Int>
    val timestamp: Flow<Long>
    val isConnected: StateFlow<Boolean>
    val deviceInfo: SharedFlow<BoomerangInfo>
    val state: StateFlow<BoomerangState>

    suspend fun connect(): Result<Unit, BoomerangException>
    suspend fun disconnect()
    suspend fun sendCmd(cmd: ByteArray): Result<Unit, BoomerangException>
    suspend fun dispose()
}