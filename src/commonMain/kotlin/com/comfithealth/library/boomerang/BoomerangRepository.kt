package com.comfithealth.library.boomerang

import com.comfithealth.library.common.DeviceRepository

class BoomerangRepository: DeviceRepository<Boomerang>()