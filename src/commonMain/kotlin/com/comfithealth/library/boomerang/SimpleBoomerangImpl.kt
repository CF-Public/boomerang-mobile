/*
 * Copyright (c) 2021, Comfit Healthcare Devices Limited
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms will only be
 * allowed with prior written permission from authorized personnel
 * in Comfit Healthcare Devices Limited
 *
 * THIS SOFTWARE IS PROVIDED BY COMFIT HEALTHCARE DEVICES LIMITED "AS IS" AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COMFIT HEALTHCARE DEVICES LIMITED OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.comfithealth.library.boomerang

import com.comfithealth.library.codec.BoomerangInfoParser
import com.comfithealth.library.codec.HeartRateParser
import com.comfithealth.library.codec.PaceParser
import com.comfithealth.library.codec.StatusParser
import com.comfithealth.library.common.BoomerangException
import com.comfithealth.library.scanning.BleAdvertisement
import com.comfithealth.library.codec.Values
import com.comfithealth.library.models.BoomerangInfo
import com.comfithealth.library.models.BoomerangState
import com.comfithealth.library.common.childScope
import com.comfithealth.library.common.describeContents
import com.comfithealth.library.common.getLong
import com.comfithealth.library.models.HeartRateData
import com.comfithealth.library.models.Status
import com.github.michaelbull.result.*
import com.juul.kable.Peripheral
import com.juul.kable.State
import com.juul.kable.characteristicOf
import com.juul.kable.peripheral
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeoutOrNull

@Suppress("EXPERIMENTAL_API_USAGE")
internal class SimpleBoomerangImpl(
    private val scope: CoroutineScope,
    advertisement: BleAdvertisement,
) : Boomerang {
    private var dataScope: CoroutineScope? = null

    override var ecg = MutableSharedFlow<ByteArray>(
        onBufferOverflow = BufferOverflow.SUSPEND,
        extraBufferCapacity = 1000
    )

    override val heartRate = MutableSharedFlow<HeartRateData>(onBufferOverflow = BufferOverflow.SUSPEND)

    override val pace = MutableSharedFlow<Int>(onBufferOverflow = BufferOverflow.SUSPEND)

    override val status = MutableSharedFlow<Status>(
        extraBufferCapacity = 1000,
        onBufferOverflow = BufferOverflow.DROP_OLDEST,
        replay = 1
    )

    override val timestamp = MutableSharedFlow<Long>(onBufferOverflow = BufferOverflow.SUSPEND)

    override val isConnected = MutableStateFlow(false)

    override val deviceInfo =
        MutableSharedFlow<BoomerangInfo>(replay = 1, onBufferOverflow = BufferOverflow.DROP_OLDEST)

    private var expectingExtendedError = false


    override val state = MutableStateFlow<BoomerangState>(BoomerangState.Disconnected)

    override val address: String = advertisement.identifier

    private val peripheral: Peripheral = scope.peripheral(advertisement.advertisement)

    private val _connection = scope.launch {
        peripheral.state.collect {
            state.value = when (it) {
                State.Connecting -> BoomerangState.Connecting
                State.Connected -> BoomerangState.Connected
                State.Disconnecting -> BoomerangState.ShuttingDown
                is State.Disconnected -> {
                    expectingExtendedError = false
                    BoomerangState.Disconnected
                }
            }
        }
    }


    override suspend fun connect(): Result<Unit, BoomerangException> {
        return runCatching {
            Napier.d("Connecting to device...")
            withTimeoutOrNull(CONNECTION_TIMEOUT_MILLIS) {
                peripheral.connect()
                onServicesDiscovered()
            } ?: return Err(BoomerangConnectionFailedError)
        }.mapError {
            Napier.e(it.stackTraceToString())
            return when (state.value is BoomerangState.Connected || state.value is BoomerangState.Connecting) {
                true -> {
                    isConnected.emit(true)
                    Ok(Unit)
                }
                else -> {
                    Napier.e("Unable to connect to Ble device due to timeout")
                    disconnect()
                    Err(BoomerangConnectionFailedError)
                }
            }
        }
    }

    override suspend fun disconnect() {
        peripheral.disconnect()
        isConnected.value = false
    }

    override suspend fun sendCmd(cmd: ByteArray): Result<Unit, BoomerangException> {
        return runCatching {
            // cancel current emissions
            // && reset flows when sending out commands
            dataScope?.cancel()
            dataScope = scope.childScope()

            ecg.resetReplayCache()
            heartRate.resetReplayCache()
            pace.resetReplayCache()

            Napier.d("Sending: ${cmd.contentToString()}")
            val characteristic = characteristicOf(Values.BOOMERANG_DEVICE_SERVICE_UUID, Values.CTRL_CHAR_UUID)
            peripheral.write(characteristic, cmd)
        }.mapError { BoomerangCmdExecFailedError }
    }

    override suspend fun dispose() {
        ecg.resetReplayCache()
        heartRate.resetReplayCache()
        pace.resetReplayCache()
        disconnect()
        _connection.cancel()
    }

    private fun observe(uuid: String, callback: suspend (ByteArray) -> Unit) =
        scope.launch {
            peripheral.observe(
                characteristicOf(Values.BOOMERANG_DEVICE_SERVICE_UUID, uuid)
            ).collect(callback)
        }


    private fun onServicesDiscovered() {
        observe(Values.STATUS_CHAR_UUID) {
            runCatching {
                val result = StatusParser.parse(it, expectingExtendedError)
                expectingExtendedError = result.extendedError
                this.status.tryEmit(result.status)
            }
        }
        observe(Values.R_VALUE_CHAR_UUID) {
            runCatching {
                heartRate.emit(HeartRateParser.parse(it))
            }
        }
        observe(Values.ECG_VALUE_CHAR_UUID) {
            runCatching {
                ecg.emit(it)
            }
        }

        observe(Values.PACE_CHAR_UUID) {
            runCatching {
                pace.emit(PaceParser.parse(it))
            }
        }

        observe(Values.DATE_TIME_CHAR_UUID) {
            runCatching {
                val timestamp = it.getLong()
                Napier.d("Date: ${it.describeContents()}, $timestamp}")
                this.timestamp.emit(timestamp)
            }
        }

        observe(Values.FLASH_CHAR_UUID) {
            runCatching {
                val info = BoomerangInfoParser.parse(it)
                deviceInfo.tryEmit(info)
                Napier.d(info.toString())
            }
        }
    }

    companion object {
        private const val CONNECTION_TIMEOUT_MILLIS = 5_000L
    }
}