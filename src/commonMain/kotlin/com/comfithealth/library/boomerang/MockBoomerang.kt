///*
// * Copyright (c) 2021, Comfit Healthcare Devices Limited
// *
// * All rights reserved.
// *
// * Redistribution and use in source and binary forms will only be
// * allowed with prior written permission from authorized personnel
// * in Comfit Healthcare Devices Limited
// *
// * THIS SOFTWARE IS PROVIDED BY COMFIT HEALTHCARE DEVICES LIMITED "AS IS" AND ANY
// * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// * WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE
// * ARE DISCLAIMED. IN NO EVENT SHALL COMFIT HEALTHCARE DEVICES LIMITED OR CONTRIBUTORS
// * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
// * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
// * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// */
//
//package com.comfithealth.library.boomerang
//
//import com.comfithealth.library.codec.CmdDecoder
//import com.comfithealth.library.common.BoomerangException
//import com.comfithealth.library.models.BoomerangInfo
//import com.comfithealth.library.models.BoomerangState
//import com.comfithealth.library.models.DeviceState
//import com.comfithealth.library.common.AssetLoader
//import com.comfithealth.library.domain.*
//import com.github.michaelbull.result.*
//import kotlinx.coroutines.*
//import kotlinx.coroutines.channels.BufferOverflow
//import kotlinx.coroutines.flow.MutableSharedFlow
//import kotlinx.coroutines.flow.MutableStateFlow
//import kotlinx.coroutines.flow.StateFlow
//
//@Suppress("EXPERIMENTAL_API_USAGE")
//class MockBoomerang constructor(
//    private val loader: AssetLoader,
//    private val scope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.Main),
//) : Boomerang {
//    var delayMillis: Long = 8
//    private var ecgBytes: ByteArray = byteArrayOf()
//    private var paceData: List<Int> = emptyList()
//    private var heartRateData: List<HeartRateData> = emptyList()
//    private var bytesSent = 0
//    private var paceIdx = 0
//    private var heartRateIdx = 0
//    var maxChunkSize = 16
//
//    private var _status = DEFAULT_STATUS
//
//    override val address: String get() = "DE:AD:BE:EE:EE:EF"
//    override val ecg = MutableSharedFlow<ByteArray>(
//        onBufferOverflow = BufferOverflow.SUSPEND,
//        extraBufferCapacity = 1000
//    )
//    override val heartRate = MutableSharedFlow<HeartRateData>()
//    override val status =
//        MutableSharedFlow<Status>(replay = 1, onBufferOverflow = BufferOverflow.DROP_OLDEST)
//    override val pace = MutableSharedFlow<Int>()
//    override val timestamp = MutableSharedFlow<Long>()
//    override val isConnected = MutableStateFlow(false)
//    override val deviceInfo =
//        MutableSharedFlow<BoomerangInfo>(replay = 1, onBufferOverflow = BufferOverflow.DROP_OLDEST)
//    override val state: StateFlow<BoomerangState>
//        get() = TODO("Not yet implemented")
//
//    private var decoder: CmdDecoder? = null
//    private var sendJob: Job? = null
//    private var loaded = false
//
//    override suspend fun connect(): Result<Unit, BoomerangException> {
//        isConnected.emit(true)
//        _status = DEFAULT_STATUS
//        status.tryEmit(_status)
//        return Ok(Unit)
//    }
//
//    override suspend fun disconnect() {
//        isConnected.emit(false)
//    }
//
//    override suspend fun sendCmd(cmd: ByteArray): Result<Unit, BoomerangException> {
//        decoder = CmdDecoder(cmd)
//        decoder?.let { parser ->
//            _status = _status.copy(
//                mode = parser.mode,
//                fingers = parser.fingerMode,
//                sampleRate = parser.sampleRate,
//                isDSPOn = parser.dspFilterOn,
//                boomerangErr = null,
//                lpFilter = parser.lpFilter,
//                finishCode = null,
//                state = when (parser.cmd) {
//                    Cmd.Start -> DeviceState.RECORDING
//                    Cmd.Stop, is Cmd.FactoryAdjust, is Cmd.SetAddress, null -> {
//                        sendJob?.cancel()
//                        DeviceState.READY
//                    }
//                    Cmd.Reset -> DeviceState.SHUTTING_DOWN
//                    Cmd.Calibrate -> DeviceState.CALIBRATING
//                    Cmd.StdOneMV -> DeviceState.STD_1MV_OUT
//                }
//            )
//            status.emit(_status)
//
//            when (parser.cmd) {
//                Cmd.Start -> {
//                    if (loaded.not()) {
//                        loadSrNormal()
//                    }
//                    loaded = false
//                    send()
//                }
//                Cmd.Stop -> {
//                    ecg.resetReplayCache()
//                    pace.resetReplayCache()
//                    heartRate.resetReplayCache()
//                }
//                Cmd.Reset -> {
//
//                }
//                Cmd.Calibrate -> {
//                    calibrate()
//                }
//                Cmd.StdOneMV -> {
//
//                }
//                is Cmd.FactoryAdjust, is Cmd.SetAddress -> {
//
//                }
//                null -> {
//                    // TODO: Send status...
//                }
//            }
//        }
//        return Ok(Unit)
//    }
//
//    override suspend fun dispose() = scope.cancel()
//
//
//    fun load(record: Record) {
//        ecgBytes = getECGArray(record)
//        paceData = record.session.pace
//        heartRateData = record.session.heartRate
//        loaded = true
//    }
//
//    fun load(data: List<Short>, heartRateData: List<HeartRateData> = emptyList()) {
//        ecgBytes = getECGArray(data)
//        this.heartRateData = heartRateData
//        loaded = true
//    }
//
//
//    internal fun load(ecgBytes: ByteArray) {
//        this.ecgBytes = ecgBytes
//        this.heartRateData = emptyList()
//        this.paceData = emptyList()
//    }
//
//    /**
//     * Real ECG recording with heart rate depicting respiration
//     */
//    fun loadSrNormal() {
//        val data = context.assetAsShortList("test/sr_normal_80bpm.csv")
//        val hr = context.assetAsIntList("test/sr_normal_80bpm_peaks.csv")
//        load(data, hr.map { HeartRateData(80, it) })
//    }
//
//    private fun send() {
//        bytesSent = 0
//        heartRateIdx = 0
//        paceIdx = 0
//
//
//        sendJob = scope.launch {
//            try {
//                while (bytesSent < ecgBytes.size && isActive) {
//                    val chunkSize = Math.min(ecgBytes.size - bytesSent, maxChunkSize)
//                    val array = ecgBytes.copyOfRange(bytesSent, bytesSent + chunkSize)
//                    bytesSent += chunkSize
//
//                    ecg.emit(array)
//                    Timber.d("Sent $chunkSize bytes")
//
//                    delay(delayMillis)
//                    while (paceIdx < paceData.size && paceData[paceIdx] < (bytesSent / 2)) {
//                        pace.emit(paceData[paceIdx++])
//                    }
//                    while (heartRateIdx < heartRateData.size && heartRateData[heartRateIdx].index < (bytesSent / 2)) {
//                        heartRate.emit(heartRateData[heartRateIdx++])
//                    }
//                }
//
//                Timber.d("Recording ended")
//                status.emit(
//                    _status.copy(
//                        boomerangErr = null,
//                        state = DeviceState.READY,
//                        finishCode = FinishCode.SUCCESS
//                    )
//                )
//            } catch (e: CancellationException) {
//                Timber.d("Recording stopped abruptly")
//            }
//        }
//    }
//
//
//    private fun calibrate() {
//        bytesSent = 0
//        ecgBytes = ByteArray(MAX_CALIBRATION_BYTE_COUNT)
//        val buffer = ByteBuffer.wrap(ecgBytes).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer()
//        for (i in 0 until (MAX_CALIBRATION_BYTE_COUNT / 2)) {
//            buffer.put(i, 400)
//        }
//
//        sendJob = scope.launch {
//            try {
//                while (bytesSent < ecgBytes.size && isActive) {
//                    val chunkSize = Math.min(ecgBytes.size - bytesSent, maxChunkSize)
//                    val array = ecgBytes.copyOfRange(bytesSent, bytesSent + chunkSize)
//                    bytesSent += chunkSize
//
//                    ecg.emit(array)
//                    Timber.d("Sent $chunkSize bytes")
//
//                    delay(delayMillis)
//                }
//
//                Timber.d("Calibration ended")
//                status.emit(
//                    _status.copy(
//                        boomerangErr = null,
//                        state = DeviceState.READY,
//                        finishCode = null
//                    )
//                )
//            } catch (e: CancellationException) {
//                Timber.d("Calibration stopped abruptly")
//            }
//        }
//    }
//
//    companion object {
//        private const val MAX_CALIBRATION_BYTE_COUNT = 2 * 12_000
//
//        val DEFAULT_STATUS = Status(
//            mode = Config.DEFAULT.mode,
//            boomerangErr = null,
//            isDSPOn = Config.DEFAULT.dspFilter,
//            finishCode = null,
//            state = DeviceState.READY,
//            fingers = Config.DEFAULT.fingerMode,
//            lpFilter = Config.DEFAULT.lpFilter,
//            sampleRate = Config.DEFAULT.sampleRate
//        )
//
//        internal fun getECGArray(ecg: List<Short>): ByteArray {
//            val bytes = ecg.size * 2
//            val array = ByteArray(bytes)
//            val buffer = ByteBuffer.wrap(array).order(ByteOrder.LITTLE_ENDIAN)
//            ecg.forEachIndexed { index, sh ->
//                buffer.putShort(index * 2, sh)
//            }
//            return array
//        }
//
//        internal fun getECGArray(record: Record): ByteArray {
//            val bytes = record.session.samples.size * 2
//            val array = ByteArray(bytes)
//            val buffer = ByteBuffer.wrap(array).order(ByteOrder.LITTLE_ENDIAN)
//            record.session.samples.forEachIndexed { index, sh ->
//                buffer.putShort(index * 2, sh)
//            }
//            return array
//        }
//    }
//}