/*
 * Copyright (c) 2021, Comfit Healthcare Devices Limited
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms will only be
 * allowed with prior written permission from authorized personnel
 * in Comfit Healthcare Devices Limited
 *
 * THIS SOFTWARE IS PROVIDED BY COMFIT HEALTHCARE DEVICES LIMITED "AS IS" AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COMFIT HEALTHCARE DEVICES LIMITED OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.comfithealth.library.codec

import com.comfithealth.library.models.*

internal object StatusParser {

    internal data class Result(val status: Status, val extendedError: Boolean)

    fun parse(value: ByteArray, expectingExtendedError: Boolean = false): Result {
        fun bool(index: Int): Boolean {
            val arrayIndex = index / 8
            val bitIndex = index % 8
            val num = (value[arrayIndex].toInt() and (1 shl bitIndex)) ushr bitIndex
            return num == 1
        }

        val stateCode = value[0].toInt() and 0x07
        val state = when (stateCode) {
            0 -> DeviceState.READY
            1 -> DeviceState.RECORDING
            2 -> DeviceState.CALIBRATING
            3 -> DeviceState.STD_1MV_OUT
            4 -> DeviceState.SHUTTING_DOWN
            5 -> DeviceState.READY // It's actually 'AutoMode Finished' in firmware source code
            else -> throw IllegalArgumentException("Unknown state code '$stateCode'")
        }


        val finishCode = when (stateCode == 5 /* AutoMode Finished */) {
            true -> FinishCode.SUCCESS
            else -> when (val finishCodeInt = (value[1].toInt() ushr 1) and 0x03) {
                0 -> null
                1 -> FinishCode.SUCCESS
                2 -> FinishCode.FAILURE
                else -> throw  IllegalArgumentException("Unknown finishCode '$finishCodeInt'")
            }
        }

        var extendedError = false
        val errorCodeInt = (value[1].toInt() ushr 3) and 0x1F
        val errorCode = when (expectingExtendedError) {
            true -> BoomerangErr.Extended(errorCodeInt)
            else -> when (errorCodeInt) {
                0 -> null // no error
                1 -> BoomerangErr.BleErr
                2 -> BoomerangErr.LeadOff
                3 -> BoomerangErr.PllFailure
                4 -> BoomerangErr.EcgSendQueue
                5 -> BoomerangErr.EcgFifoEmpty
                6 -> BoomerangErr.EcgFifoOverflown
                7 -> BoomerangErr.BiozFifoEmpty
                8 -> BoomerangErr.BiozFifoOverflown
                9 -> BoomerangErr.EcgFifoRead
                10 -> BoomerangErr.InsufficientReadTime
                11 -> BoomerangErr.TimeoutShutdown
                12 -> BoomerangErr.ExtendedLeadOffShutdown
                13 -> BoomerangErr.LowBattery
                14 -> BoomerangErr.DangerouslyLowBattery
                15 -> BoomerangErr.Speaker
                16 -> BoomerangErr.RealTimeClock
                17 -> BoomerangErr.SensorCalibration
                18 -> BoomerangErr.MaxRetryExceeded
                30 -> {
                    extendedError = true
                    null
                }
                else -> BoomerangErr.Unknown
            }
        }

        val sampleRate = when (bool(3)) {
            true -> SampleRate.R_512
            else -> SampleRate.R_256
        }

        val lpFilter = when (val lpFilterCode = (value[0].toInt() shr 5) and 0x3) {
            0 -> LPFilter.Off
            1 -> LPFilter.F40
            2 -> LPFilter.F100
            else -> throw IllegalArgumentException("Unknown LPFilter code '$lpFilterCode'")
        }

        val mode = when (bool(7)) {
            true -> Mode.Auto
            else -> Mode.Manual
        }
        val fingers = when (bool(8)) {
            true -> FingerMode.Two
            else -> FingerMode.Three
        }

        val isDSPOn = bool(4)

        return Result(
            Status(
                mode = mode,
                sampleRate = sampleRate,
                value = value,
                lpFilter = lpFilter,
                boomerangErr = errorCode,
                fingers = fingers,
                finishCode = finishCode,
                isDSPOn = isDSPOn,
                state = state
            ),
            extendedError
        )
    }
}

