/*
 * Copyright (c) 2021, Comfit Healthcare Devices Limited
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms will only be
 * allowed with prior written permission from authorized personnel
 * in Comfit Healthcare Devices Limited
 *
 * THIS SOFTWARE IS PROVIDED BY COMFIT HEALTHCARE DEVICES LIMITED "AS IS" AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COMFIT HEALTHCARE DEVICES LIMITED OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.comfithealth.library.codec

import com.comfithealth.library.models.BoomerangInfo
import com.comfithealth.library.common.getInt
import com.comfithealth.library.common.getLong


internal object BoomerangInfoParser {
    private const val CALIBRATE_MAGIC = 0x20170317

     fun parse(value: ByteArray): BoomerangInfo {
        val sdk = run {
            // e.g. 0x1530 ==> 15.3.0
            val x =  value.getInt(0) //iBuf.get(0)
            "${((x ushr 8) and 0xFF).bcdToBin()}.${(x ushr 4) and 0x0F}.${x and 0x0F}"
        }
        val firmwareVersion = run {
            // e.g. 0x21021001  ==> Year: (20)21, Month: 02, DAY: 10;  revision: 01
            val x =  value.getInt(1 * 4 * Int.SIZE_BYTES) // iBuf.get(1 * 4)
            val year = ((x ushr 24) and 0xFF).bcdToBin() + 2000
            val month = ((x ushr 16) and 0xFF).bcdToBin()
            val day = ((x ushr 8) and 0xFF).bcdToBin()
            val revision = (x and 0xFF).bcdToBin()
            "$year.${month.format(2)}.${day.format(2)}.${revision.format(2)}"
        }

        val calibrationBaseline = value.getInt(2 * 4 * Int.SIZE_BYTES)  // iBuf.get(2 * 4)
        val calibrationAmplitude = value.getInt(3 * 4 * Int.SIZE_BYTES) // iBuf.get(3 * 4)
        val calibrationTime =
            value.copyOfRange(16 * 4, 24 * 4).getLong() // iBuf[4] and iBuf[5]
        val isCalibrated = value.getInt(24 * Int.SIZE_BYTES) == CALIBRATE_MAGIC // iBuf.get(24) == CALIBRATE_MAGIC

        val lastErrorTimestamp =
            value.copyOfRange(28 * 4, 36 * 4).getLong() // iBuf[7] and iBuf[8]
        val lastError = value.getInt(9 * 4 * Int.SIZE_BYTES)  // iBuf.get(9 * 4)

        return BoomerangInfo(
            sdkVersion = sdk,
            firmwareVersion = firmwareVersion,
            isCalibrated = isCalibrated,
            lastCalibrationTime = calibrationTime,
            calibrationBaseline = calibrationBaseline,
            calibrationAmplitude = calibrationAmplitude,
            lastErrorCodeTime = lastErrorTimestamp,
            lastErrorCode = lastError
        )
    }

    private fun Int.bcdToBin() = this - 6 * (this ushr 4)

    private fun Int.format(digits: Int) =  this.toString().padStart(digits, '0') // "%0${digits}d".format(this)
}