/*
 * Copyright (c) 2021, Comfit Healthcare Devices Limited
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms will only be
 * allowed with prior written permission from authorized personnel
 * in Comfit Healthcare Devices Limited
 *
 * THIS SOFTWARE IS PROVIDED BY COMFIT HEALTHCARE DEVICES LIMITED "AS IS" AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COMFIT HEALTHCARE DEVICES LIMITED OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.comfithealth.library.codec

import com.comfithealth.library.common.getInt
import com.comfithealth.library.common.toBytes
import com.comfithealth.library.models.*

///
/// BYTE 0
///
///        7            6            5            4            3            2            1           0
/// +------------+------------+-------------+------------+------------+------------+------------+------------+
/// +  LPF 40HZ  + DSP FILTER + SAMPLE RATE +   START    +  AUTO CAL  +   ONE MV   + DISCONNECT +    STOP    +
/// +------------+------------+-------------+------------+------------+------------+------------+------------+
///
///
/// BYTE 1
///
///       15           14            13           12            11           10           9           8
/// +------------+-----------+------------+----------------+------------+------------+------------+------------+
/// +  ADJ VAL   +  ADJUST   + RA/LA SWAP + GET FLASH DATA +    RESET   +  FINGERS   +    MODE    + LPF 100HZ  +
/// +------------+-----------+------------+----------------+------------+------------+------------+------------+
///
///
/// BYTE 2
///
///        23           22           21           20           19          18         17          16
/// +------------+-----------+------------+--------------+------------+------------+--------+-----------------+
/// + SAVE MAC   +                                        ADJ VAL                                             +
/// +------------+-----------+------------+--------------+------------+------------+--------+-----------------+
///
///
/// BYTE 3
///
///        31           30            29           28            27           26        25           24
/// +------------+-----------+------------+--------------+------------+------------+--------+-----------------+
/// +                                       MAC ADDRESS SEGMENT                             +   SAVE  MAC     +
/// +------------+-----------+------------+--------------+------------+------------+--------+-----------------+
private const val STOP = 1 // 0
private const val DISCONNECT = 2 // 1
private const val STD_1MV = 4 // 2

private const val AUTO_CAL = 8 // 3
private const val AUTO_CAL_OFF = AUTO_CAL.inv() // NOT

private const val START = 16 // 4

private const val RATE_512 = 32 // 5
private const val RATE_256 = RATE_512.inv() // AND

private const val DSP_FILTER_ON = 64 // 6
private const val DSP_FILTER_OFF = DSP_FILTER_ON.inv() // AND

private const val LPF_40HZ = 128 // 7-8
private const val LPF_100HZ = 256
private const val LPF_150HZ = 128 + 256
private const val LPF_OFF = (128 + 256).inv() // AND

private const val AUTO_MODE = 512 // 9
private const val MANUAL_MODE = AUTO_MODE.inv() // AND

private const val TWO_FINGERS = 1024 // 10
private const val THREE_FINGERS = TWO_FINGERS.inv() // AND

private const val RESET = 2048 // 11
private const val GET_FLASH_DATA = 4096 // 12
private const val ARM_SWAP = 8192 // 13
private const val ARM_NO_SWAP = ARM_SWAP.inv() // AND


private const val FACTORY_ADJUST = 16384 // 14

internal class CmdEncoder {
    private var value = 0x00000000

    fun stop() = or(STOP).build()

    fun disconnect() = or(DISCONNECT).build()

    fun std1MV() = or(STD_1MV).build()

    fun start() = or(START).build()

    fun reset() = or(RESET).build()

    fun readFlashData() = or(GET_FLASH_DATA).build()

    fun calibration(auto: Boolean = true) = when (auto) {
        true -> or(AUTO_CAL)
        false -> and(AUTO_CAL_OFF)
    }

    fun sampleRate(is512: Boolean = false) = when (is512) {
        true -> or(RATE_512)
        else -> and(RATE_256)
    }

    fun dspFilter(on: Boolean = false) = when (on) {
        true -> or(DSP_FILTER_ON)
        else -> and(DSP_FILTER_OFF)
    }

    fun lpFilter(lpFilter: LPFilter) = when (lpFilter) {
        LPFilter.F40 -> and(LPF_OFF).or(LPF_40HZ)
        LPFilter.F100 -> and(LPF_OFF).or(LPF_100HZ)
        LPFilter.Off -> and(LPF_OFF)
        LPFilter.F150 -> or(LPF_150HZ)
    }

    fun mode(mode: Mode) = when (mode) {
        Mode.Auto -> or(AUTO_MODE)
        Mode.Manual -> and(MANUAL_MODE)
    }

    fun fingers(fingerMode: FingerMode) = when (fingerMode) {
        FingerMode.Two -> or(TWO_FINGERS)
        FingerMode.Three -> and(THREE_FINGERS)
    }

    fun swap(armSwap: ArmSwap) = when (armSwap) {
        ArmSwap.NoSwap -> and(ARM_NO_SWAP)
        ArmSwap.Swap -> or(ARM_SWAP)
    }

    fun factoryAdjust(adjVal: Int): ByteArray {
        value = value or FACTORY_ADJUST
        value = value or ((adjVal and 0xFF) shl 15)
        return build()
    }

    /**
     * Encode a segment of mac address to send.
     *
     * Boomerang has a MAC address of the form 0x6015925XXXXX where the starting sequence '6015925'
     * is the Company's identifier and the remaining 5 nibbles marked as 'XXXXX' is unique for every
     * Boomerang.
     *
     * The 5 nibbles is not sent as a whole. It is split into 3 segments; [SEGMENT_00_06],
     * [SEGMENT_07_13], and [SEGMENT_14_20].
     *
     * @param segment - segment
     * @param data - segment data
     */
    fun macAddress(@Segment segment: Int, data: Int): ByteArray {
        require(segment in intArrayOf(SEGMENT_00_06, SEGMENT_07_13, SEGMENT_14_20))
        value = value or ((segment and 0x3) shl 23)
        value = value or ((data and 0x7F) shl 25)
        return build()
    }

    fun macAddress(@Segment segment: Int, address: String): ByteArray {
        val value = address.replace(":", "").toLong(16)
            .and(0xFFFFF)
            .toInt()
        return macAddress(segment, value ushr ((segment - 1) * 7))
    }

    private fun or(value: Int): CmdEncoder {
        this.value = this.value or value
        return this
    }

    private fun and(value: Int): CmdEncoder {
        this.value = this.value and value
        return this
    }

    fun build() = value.toBytes()

    annotation class Segment


    companion object {
        /**
         *  From bit 0 to bit 6
         */
        const val SEGMENT_00_06 = 1

        /**
         * From bit 7 to bit 13
         */
        const val SEGMENT_07_13 = 2

        /**
         * From bit 14 to bit 20
         */
        const val SEGMENT_14_20 = 3
    }
}

internal class CmdDecoder(command: ByteArray) {
    val value: Int = command.getInt()

    val cmd: Cmd? = when {
        value and STOP == STOP -> Cmd.Stop
        value and STD_1MV == STD_1MV -> Cmd.StdOneMV
        value and AUTO_CAL == AUTO_CAL -> Cmd.Calibrate
        value and START == START -> Cmd.Start
        value and RESET == RESET -> Cmd.Reset
        value and DISCONNECT == DISCONNECT -> null // TODO: Implement disconnect
        else -> null
    }

    val sampleRate: SampleRate = when {
        value and RATE_256 == RATE_256 -> SampleRate.R_256
        else -> SampleRate.R_512
    }

    val mode: Mode = if (value and AUTO_MODE == AUTO_MODE) {
        Mode.Auto
    } else {
        Mode.Manual
    }

    val lpFilter: LPFilter = when {
        value and LPF_150HZ == LPF_150HZ -> LPFilter.F150
        value and LPF_100HZ == LPF_100HZ -> LPFilter.F100
        value and LPF_40HZ == LPF_40HZ -> LPFilter.F40
        else -> LPFilter.Off
    }

    val fingerMode: FingerMode = when {
        value and TWO_FINGERS == TWO_FINGERS -> FingerMode.Two
        else -> FingerMode.Three
    }

    val swap: ArmSwap = when {
        value and ARM_SWAP == ARM_SWAP -> ArmSwap.Swap
        else -> ArmSwap.NoSwap
    }

    val dspFilterOn: Boolean = value and DSP_FILTER_ON == DSP_FILTER_ON
}

