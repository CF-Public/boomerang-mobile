package com.comfithealth.library.common


expect class Platform() {
    val platform: String
}

internal expect suspend fun sleep(millis: Long, nanos: Int)