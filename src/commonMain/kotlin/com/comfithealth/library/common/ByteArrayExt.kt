/*
 * Copyright (c) 2021, Comfit Healthcare Devices Limited
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms will only be
 * allowed with prior written permission from authorized personnel
 * in Comfit Healthcare Devices Limited
 *
 * THIS SOFTWARE IS PROVIDED BY COMFIT HEALTHCARE DEVICES LIMITED "AS IS" AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COMFIT HEALTHCARE DEVICES LIMITED OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
@file:Suppress("EXPERIMENTAL_API_USAGE")

package com.comfithealth.library.common


fun ByteArray.reset() {
    fill(0.toByte(), 0, size)
}

fun ByteArray.toShortArray() = asList()
    .chunked(2)
    .map { (l, h) -> (l.toInt() + h.toInt().shl(8)).toShort() }
    .toShortArray()


fun ByteArray.getLong(offsetInBytes: Int = 0): Long {
    require(size >= offsetInBytes + Long.SIZE_BYTES)
    var num = 0L
    for (index in offsetInBytes until (offsetInBytes + Long.SIZE_BYTES)) {
        val byte = this[index]
        num = num or (byte.toUByte().toLong() shl (index * 8))
    }
    return num
}

fun ByteArray.getInt(offsetInBytes: Int = 0): Int {
    require(size >= offsetInBytes + Int.SIZE_BYTES)
    var num = 0
    for (index in offsetInBytes until (offsetInBytes + Int.SIZE_BYTES)) {
        val byte = this[index]
        num = num or (byte.toUByte().toInt() shl (index * 8))
    }
    return num
}

fun ByteArray.getShort(offsetInBytes: Int = 0): Short {
    require(size >= offsetInBytes + Short.SIZE_BYTES)
    var num = 0
    for (index in offsetInBytes until (offsetInBytes + Short.SIZE_BYTES)) {
        val byte = this[index]
        num = num or (byte.toUByte().toInt() shl (index * 8))
    }
    return num.toShort()
}

fun ByteArray.toHexString() = asUByteArray().joinToString("") { it.toString(16).padStart(2, '0') }
fun ByteArray.describeContents() = joinToString(", ") { toHexString() }

