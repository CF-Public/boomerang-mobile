package com.comfithealth.library.common

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

abstract class DeviceRepository<T : Any> {
    private val _picked = MutableStateFlow<T?>(null)

    val picked: StateFlow<T?> = _picked

    fun pick(boomerang: T) {
        _picked.value = boomerang
    }

    fun clear() {
        _picked.value = null
    }
}