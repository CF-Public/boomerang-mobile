package com.comfithealth.library.service

import com.comfithealth.library.common.DeviceRepository

class BoomerangServiceRepository : DeviceRepository<BoomerangService>()