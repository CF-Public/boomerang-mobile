/*
 * Copyright (c) 2021, Comfit Healthcare Devices Limited
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms will only be
 * allowed with prior written permission from authorized personnel
 * in Comfit Healthcare Devices Limited
 *
 * THIS SOFTWARE IS PROVIDED BY COMFIT HEALTHCARE DEVICES LIMITED "AS IS" AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COMFIT HEALTHCARE DEVICES LIMITED OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.comfithealth.library.service

import com.comfithealth.library.common.BoomerangException
import com.comfithealth.library.boomerang.Boomerang
import com.comfithealth.library.models.BoomerangEvent
import com.comfithealth.library.models.BoomerangState
import com.comfithealth.library.models.Cmd
import com.comfithealth.library.models.Config
import com.comfithealth.library.models.Evt
import com.github.michaelbull.result.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

/**
 * Utilizing [Boomerang] directly for reading ecg data can be tedious.
 *
 * Implementations of [BoomerangService] help simplify usage by providing
 *  - Signal processing,
 *  - Ensuring the ordering of ECG, Heart Rate and Pace data information.
 *  - State management,
 *  - and other custom behaviour as defined in [Config]
 */
interface BoomerangService {
    val state: StateFlow<BoomerangState>
    val event: Flow<BoomerangEvent>
    val evt: Flow<Evt>
    val config: Config
    val isConnected: StateFlow<Boolean>
    val address: String
    suspend fun setConfig(config: Config): Result<Unit, BoomerangException>
    suspend fun connect(): Result<Unit, BoomerangException>
    suspend fun disconnect()
    suspend fun execute(cmd: Cmd): Result<Unit, BoomerangException>
    suspend fun dispose()
}