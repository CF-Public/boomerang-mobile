/*
 * Copyright (c) 2021, Comfit Healthcare Devices Limited
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms will only be
 * allowed with prior written permission from authorized personnel
 * in Comfit Healthcare Devices Limited
 *
 * THIS SOFTWARE IS PROVIDED BY COMFIT HEALTHCARE DEVICES LIMITED "AS IS" AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COMFIT HEALTHCARE DEVICES LIMITED OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.comfithealth.library.service

import com.comfithealth.library.boomerang.Boomerang
import com.comfithealth.library.boomerang.BoomerangDisconnectedError
import com.comfithealth.library.boomerang.BoomerangNotSetError
import com.comfithealth.library.codec.CmdEncoder
import com.comfithealth.library.common.BoomerangException
import com.comfithealth.library.service.EcgComponentActor.Msg
import com.comfithealth.library.common.childScope
import com.comfithealth.library.common.describeContents
import com.comfithealth.library.models.*
import com.github.michaelbull.result.*
import io.github.aakira.napier.Napier
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.*

internal class SimpleBoomerangServiceImpl(
    private val scope: CoroutineScope,
    private val boomerang: Boomerang,
    private val ecgComponentActorFactory: EcgComponentActor.Factory,
    override var config: Config,
) : BoomerangService {

    private var isPendingStateReset = false
    private var wasConnected = false
    private var deviceState: DeviceState? = null

    private var boomerangScope: CoroutineScope? = null
    private var dataScope: CoroutineScope? = null
    private var ecgComponentActor: EcgComponentActor? = null

    override val evt = MutableSharedFlow<Evt>(
        replay = 1024,
        extraBufferCapacity = 1024,
        onBufferOverflow = BufferOverflow.DROP_OLDEST
    )

    override val isConnected: StateFlow<Boolean> = boomerang.isConnected

    override val state = MutableStateFlow<BoomerangState>(BoomerangState.Disconnected)

    override val event = MutableSharedFlow<BoomerangEvent>(
        extraBufferCapacity = 1_024,
        onBufferOverflow = BufferOverflow.DROP_OLDEST
    )

    override val address: String = boomerang.address

    override suspend fun setConfig(config: Config): Result<Unit, BoomerangException> {
        this.config = config
        if (boomerang.isConnected.value) {
            unsubscribeFromEcgComponents()
            isPendingStateReset = true
            return sendConfig()
                .onSuccess {
                    subscribeToEcgComponents()
                }
        }
        return Ok(Unit)
    }

    override suspend fun connect(): Result<Unit, BoomerangException> {
        // observe device's properties
        boomerangScope = scope.childScope()
        boomerangScope?.let {
            it.launch(Dispatchers.Main) {
                boomerang.isConnected.collect { isConnected ->
                    if (isConnected) {
                        state.value = BoomerangState.Connected
                        wasConnected = true
                        evt.emit(Evt.Connected)
                    } else {
                        state.value = BoomerangState.Disconnected
                        Napier.d("Disconnected from Boomerang")
                        evt.emit(Evt.Disconnected)
                        if (wasConnected) {
                            boomerangScope?.cancel()
                        }
                    }
                }
            }
            it.launch(Dispatchers.Main) {
                boomerang.status.collect(::onStatus)
            }
        }

        return boomerang.connect()
            .flatMap {
                if (config.mode == Mode.Auto) {
                    subscribeToEcgComponents()
                }
                sendConfig()
            }
    }

    override suspend fun disconnect() {
        boomerang.disconnect()
        unsubscribeFromEcgComponents()
    }

    override suspend fun execute(cmd: Cmd): Result<Unit, BoomerangException> =
        boomerang.toResultOr { BoomerangNotSetError }
            .flatMap {
                if (it.isConnected.value) {
                    Ok(it)
                } else {
                    Err(BoomerangDisconnectedError)
                }
            }.flatMap { boomerang ->
                when (cmd) {
                    is Cmd.FactoryAdjust -> boomerang.sendCmd(CmdEncoder().factoryAdjust(cmd.value))
                    is Cmd.SetAddress ->
                        boomerang.sendCmd(
                            CmdEncoder().macAddress(CmdEncoder.SEGMENT_00_06, cmd.address)
                        ).flatMap {
                            boomerang.sendCmd(
                                CmdEncoder().macAddress(
                                    CmdEncoder.SEGMENT_07_13,
                                    cmd.address
                                )
                            )
                        }.flatMap {
                            boomerang.sendCmd(
                                CmdEncoder().macAddress(
                                    CmdEncoder.SEGMENT_14_20,
                                    cmd.address
                                )
                            )
                        }
                    Cmd.Reset -> boomerang.sendCmd(CmdEncoder().reset())
                    Cmd.Stop -> {
                        boomerang.sendCmd(CmdEncoder().stop())
                            .onSuccess {
                                /**
                                 * send [Msg.StopImmediate] and when [Evt.UserStopped] is received,
                                 * [unsubscribeFromEcgComponents] is called
                                 */
                                ecgComponentActor?.send(Msg.StopImmediate)
                                Napier.d("Msg.StopImmediate")
                            }.onFailure {
                                Napier.d("Failed to execute command")
                                unsubscribeFromEcgComponents()
                            }
                    }
                    Cmd.StdOneMV, Cmd.Start, Cmd.Calibrate -> {
                        isPendingStateReset = true
                        when (cmd) {
                            Cmd.Start -> boomerang.sendCmd(getCmd().start())
                            Cmd.Calibrate -> boomerang.sendCmd(getCmd().calibration(true).build())
                            else -> boomerang.sendCmd(getCmd().std1MV())
                        }.onSuccess {
                            subscribeToEcgComponents(cmd is Cmd.Start)
                        }
                    }
                }
            }

    override suspend fun dispose() {
        unsubscribeFromConnection()
        boomerang.dispose()
    }

    private suspend fun onStatus(status: Status) {
        if (status.boomerangErr is BoomerangErr.LeadOff) {
            ecgComponentActor?.send(Msg.LeadOffStatus(isLeadOff = true))
        } else if (status.boomerangErr == null) {
            ecgComponentActor?.send(Msg.LeadOffStatus(isLeadOff = false))
        }

        when (status.finishCode) {
            FinishCode.SUCCESS -> {
                evt.emit(Evt.RecordingDone)
                ecgComponentActor?.send(Msg.RecordingDone)
            }
            FinishCode.FAILURE -> {
                evt.emit(Evt.RecordingFailed)
            }
            else -> {

            }
        }

        if (status.finishCode != FinishCode.SUCCESS) {
            state.value = when (status.state) {
                DeviceState.READY -> BoomerangState.Ready
                DeviceState.RECORDING -> BoomerangState.Recording.Normal(status.boomerangErr is BoomerangErr.LeadOff)
                DeviceState.CALIBRATING -> BoomerangState.Recording.Calibration
                DeviceState.STD_1MV_OUT -> BoomerangState.Recording.Std1Mv
                DeviceState.SHUTTING_DOWN -> BoomerangState.ShuttingDown
            }
        }

        // Tell observer, that data source has been reset due to configuration change
        // or new recording session
        if (isPendingStateReset) {
            isPendingStateReset = false
            evt.emit(Evt.Reset(config))
        }

        if (status.finishCode != FinishCode.SUCCESS) {
            evt.emit(Evt.Status(state = status.state, boomerangErr = status.boomerangErr))
        }
        deviceState = status.state
    }

    private fun subscribeToEcgComponents(isNormalRecordingMode: Boolean = true) {
        if (dataScope?.isActive == true) {
            Napier.e("Error. Attempting to subscribe to data flows again")
            return
        }
        dataScope = boomerangScope?.childScope()
        dataScope?.let {
            ecgComponentActor =
                ecgComponentActorFactory.create(
                    it,
                    config,
                    isNormalRecordingMode,
                    object : EcgComponentActor.Listener {
                        override suspend fun onEvent(evt: Evt) {
                            this@SimpleBoomerangServiceImpl.evt.emit(evt)
                            if (evt is Evt.UserStopped) {
                                state.value = BoomerangState.Finished(evt.data, true)
                                unsubscribeFromEcgComponents()
                            } else if (evt is Evt.Finished) {
                                state.value = BoomerangState.Finished(evt.data, false)
                            }
                        }

                        override suspend fun onManualModeDurationExceeded() {
                            scope.launch { execute(Cmd.Stop) }
                        }
                    }
                )

            it.launch(Dispatchers.Main) {
                boomerang.ecg.collect { data ->
                    ecgComponentActor?.send(Msg.Ecg(data))
                }
            }
            it.launch(Dispatchers.Main) {
                boomerang.heartRate.collect { data ->
                    ecgComponentActor?.send(Msg.Hr(data))
                }
            }
            it.launch(Dispatchers.Main) {
                boomerang.pace.collect { data ->
                    ecgComponentActor?.send(Msg.Pace(data))
                }
            }
        } ?: run {
            Napier.e("Unable to subscribe to ECG components")
        }
    }

    private fun unsubscribeFromEcgComponents() {
        dataScope?.cancel()
        ecgComponentActor?.close()
        dataScope = null
        ecgComponentActor = null
    }

    private fun unsubscribeFromConnection() {
        boomerangScope?.cancel()
        unsubscribeFromEcgComponents()
    }


    private suspend fun sendConfig(): Result<Unit, BoomerangException> {
        val cmd = getCmd().build()
        Napier.d("Sending Command: ${cmd.describeContents()}, config=${config}")
        return boomerang.sendCmd(cmd)
    }

    private fun getCmd() = CmdEncoder()
        .lpFilter(config.lpFilter)
        .fingers(config.fingerMode)
        .mode(config.mode)
        .swap(config.armSwap)
        .sampleRate(is512 = config.sampleRate == SampleRate.R_512)


}
