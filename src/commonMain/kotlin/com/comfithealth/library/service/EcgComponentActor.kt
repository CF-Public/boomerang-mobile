/*
 * Copyright (c) 2021, Comfit Healthcare Devices Limited
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms will only be
 * allowed with prior written permission from authorized personnel 
 * in Comfit Healthcare Devices Limited
 *
 * THIS SOFTWARE IS PROVIDED BY COMFIT HEALTHCARE DEVICES LIMITED "AS IS" AND ANY 
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL COMFIT HEALTHCARE DEVICES LIMITED OR CONTRIBUTORS 
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.comfithealth.library.service

import com.comfithealth.library.codec.Values
import com.comfithealth.library.common.sleep
import com.comfithealth.library.common.toShortArray
import com.comfithealth.library.dsp.DSPKit
import com.comfithealth.library.models.*
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock

@Suppress("EXPERIMENTAL_API_USAGE")
internal class EcgComponentActor constructor(
    private val dspKit: DSPKit,
    private val scope: CoroutineScope,
    private val config: Config,
    private val isNormalRecordingMode: Boolean,
    private val listener: Listener,
) {
    private var ecg = mutableListOf<Short>()
    private val heartRateValues = mutableListOf<HeartRateData>()
    private val paceValues = mutableListOf<Int>()

    private var filterWindowStart = 0
    private var hrIndex = 0
    private var paceIdx = 0
    private var ecgIndex = 0
    private var isLeadOff = false
    private var stopTriggered = false

    /**
     * This is usually zero except when [hrDetectionRequired] is initially true.
     */
    private var ecgIndexOffset = 0

    /**
     * Is true if 1st Heart Rate peak data is required before sending ECG samples
     */
    private var hrDetectionRequired =
        config.manualModeDuration == ManualModeDuration.Sec25 && config.mode == Mode.Manual

    // for debugging
    private var time = Clock.System.now().toEpochMilliseconds()

    private val msgActor = Channel<Msg>(capacity = Channel.UNLIMITED)

    private val bufferActor = Channel<Evt>(capacity = Channel.UNLIMITED)

    private val actorJob = scope.launch {
        for (msg in msgActor) {
            when (msg) {
                is Msg.Ecg -> {
                    ecg.addAll(msg.data.toShortArray().toList())

                    dspKit.touchQuality(ecg, config.sampleRate.value, isLeadOff)?.let {
                        bufferActor.send(Evt.Touch(it))
                    }

                    if (!hrDetectionRequired) {
                        sendEcg()
                        stopIfNecessary()
                    }
                }
                is Msg.Hr -> {
                    if (!hrDetectionRequired) {
                        heartRateValues.add(
                            HeartRateData(
                                msg.data.value,
                                msg.data.index + ecgIndexOffset
                            )
                        )
                        sendHeartRate()
                    } else {
                        hrDetectionRequired = false

                        val hr = msg.data

                        Napier.d("1st R peak detected: Discarding ${hr.index} samples. ${ecg.size - hr.index} samples left")

                        ecgIndexOffset = -hr.index
                        ecg = ecg.subList(hr.index, ecg.size)
                        heartRateValues.add(
                            HeartRateData(
                                msg.data.value,
                                msg.data.index + ecgIndexOffset
                            )
                        )

                        sendEcg()
                        stopIfNecessary()
                    }
                }
                is Msg.Pace -> {
                    paceValues.add(msg.data + ecgIndexOffset)
                    if (!hrDetectionRequired) {
                        sendPace()
                    }
                }
                is Msg.LeadOffStatus -> {
                    isLeadOff = msg.isLeadOff
                }
                Msg.RecordingDone, Msg.StopImmediate -> {
                    if (config.dspFilter) {
                        ecgFilteringSend(
                            isRecordingDone = true,
                            userStopped = msg == Msg.StopImmediate
                        )
                    } else {
                        noFilterFinish(userStopped = msg == Msg.StopImmediate)
                    }
                }
            }
        }
    }

    private val bufferJob = scope.launch {
        val rate = config.sampleRate.value
        val millis = (1e3 / rate).toLong()
        val nanos = (1_000_000 * (1e3 - millis * rate) / 256).toInt()
        for (ecg in bufferActor) {
            listener.onEvent(ecg)
            if (ecg is Evt.DelayedSample) {
                sleep(millis, nanos)
            }
        }
    }

    suspend fun send(msg: Msg) = msgActor.send(msg)

    fun close(cause: Throwable? = null) {
        bufferJob.cancel()
        actorJob.cancel()
        msgActor.close(cause)
        bufferActor.close(cause)
    }

    private suspend fun sendEcg() {
        when {
            config.dspFilter -> ecgFilteringSend()
            else -> ecgNoFilteringSend()
        }
    }


    /**
     * Filter a slice (window) of sample data... Maximum length of slice is [WINDOW_SIZE].
     */
    private suspend fun ecgFilteringSend(
        isRecordingDone: Boolean = false,
        userStopped: Boolean = false,
    ) {
        val diff = ecg.size - filterWindowStart
        if (diff >= WINDOW_SIZE || (isRecordingDone && diff > 0)) {
            val windowSize = diff.coerceAtMost(WINDOW_SIZE)
            val samples = ecg.subList(filterWindowStart, windowSize + filterWindowStart)
            filterWindowStart += windowSize
            Napier.d("Filtering from ${filterWindowStart - windowSize} to ${filterWindowStart}. ${samples.size} samples.")

            val toFilter = samples.toShortArray()
            scope.launch {
                for (sample in dspKit.filter(toFilter)) {
                    ecgIndex++
                    bufferActor.send(Evt.DelayedSample(sample))
                    sendHeartRate()
                    sendPace()
                }

                if (isRecordingDone) {
                    filterFinish(userStopped)
                }
            }
        } else if (isRecordingDone) {
            scope.launch {
                sendHeartRate()
                sendPace()
                filterFinish(userStopped)
            }
        }
    }

    private suspend fun ecgNoFilteringSend() {
        val diff = ecg.size - ecgIndex
        if (diff > 0) {
            val samples = ecg.subList(ecgIndex, diff + ecgIndex)
            for (sample in samples) {
                ecgIndex++
                bufferActor.send(Evt.Sample(sample))
                sendHeartRate()
                sendPace()
            }

            time = Clock.System.now().toEpochMilliseconds()
            Napier.d(
                "Adding $diff unfiltered samples. Waited for: ${
                    Clock.System.now().toEpochMilliseconds() - time
                } ms"
            )
        }
    }


    private suspend fun filterFinish(userStopped: Boolean) {
        if (ecg.isNotEmpty()) {
            val result = dspKit.analyze(ecg.toShortArray())
            val data = RecordSession(
                ecg,
                result.getData().toList(),
                paceValues,
                heartRateValues,
                result.isNoisy(),
                config,
            )
            bufferActor.send(
                if (userStopped) {
                    Evt.UserStopped(data)
                } else {
                    Evt.Finished(data)
                }
            )
        }
    }

    private suspend fun noFilterFinish(userStopped: Boolean) {
        val result = dspKit.analyze(ecg.toShortArray())
        val data = RecordSession(
            ecg,
            result.getData().toList(),
            paceValues,
            heartRateValues,
            result.isNoisy(),
            config,
        )
        bufferActor.send(
            if (userStopped) {
                Evt.UserStopped(data)
            } else {
                Evt.Finished(data)
            }
        )
    }

    private suspend fun stopIfNecessary() {
        val duration = config.manualModeDuration.value
        val sampleRate = config.sampleRate.value
        val seconds = ecg.size / sampleRate
        if (!stopTriggered && config.manualModeDuration != ManualModeDuration.Infinite && seconds >= duration && config.mode == Mode.Manual && isNormalRecordingMode) {
            stopTriggered = true
            listener.onManualModeDurationExceeded()
        }
    }

    private suspend fun sendHeartRate() {
        while (hrIndex < heartRateValues.size && (heartRateValues[hrIndex].index < ecgIndex)) {
            bufferActor.send(Evt.HeartRate(heartRateValues[hrIndex]))
            hrIndex++
        }
    }

    private suspend fun sendPace() {
        while (paceIdx < paceValues.size && (paceValues[paceIdx] < ecgIndex)) {
            bufferActor.send(Evt.Pace(paceValues[paceIdx]))
            paceIdx++
        }
    }


    internal sealed class Msg {
        override fun toString(): String = this::class.simpleName!!
        class Ecg(val data: ByteArray) : Msg()
        class Hr(val data: HeartRateData) : Msg()
        class Pace(val data: Int) : Msg()
        object RecordingDone : Msg()
        object StopImmediate : Msg()
        class LeadOffStatus(val isLeadOff: Boolean) : Msg()
    }

    interface Listener {
        suspend fun onEvent(evt: Evt)
        suspend fun onManualModeDurationExceeded()
    }

    internal fun interface Factory {
        fun create(
            scope: CoroutineScope,
            config: Config,
            isNormalRecordingMode: Boolean,
            listener: Listener,
        ): EcgComponentActor
    }

    companion object {
        private const val WINDOW_SIZE = Values.ECG_FILTER_WINDOW_SIZE
    }
}