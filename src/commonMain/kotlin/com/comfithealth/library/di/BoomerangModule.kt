package com.comfithealth.library.di

import com.comfithealth.library.boomerang.BoomerangFactory
import com.comfithealth.library.service.BoomerangServiceFactory
import com.comfithealth.library.service.EcgComponentActor
import com.comfithealth.library.models.Config
import com.comfithealth.library.dsp.DSPKit
import kotlinx.coroutines.CoroutineScope
import org.koin.dsl.module

val boomerangModule = module {
    single { BoomerangFactory() }
    single { BoomerangServiceFactory(get()) }
    single { DSPKit() }
    single<EcgComponentActor.Factory> { EcgComponentActorFactoryImpl(get()) }
}

internal class EcgComponentActorFactoryImpl(private val dspKit: DSPKit) : EcgComponentActor.Factory {
    override fun create(
        scope: CoroutineScope,
        config: Config,
        isNormalRecordingMode: Boolean,
        listener: EcgComponentActor.Listener
    ): EcgComponentActor = EcgComponentActor(dspKit, scope, config, isNormalRecordingMode, listener)
}