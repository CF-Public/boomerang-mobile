/*
 * Copyright (c) 2021, Comfit Healthcare Devices Limited
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms will only be
 * allowed with prior written permission from authorized personnel
 * in Comfit Healthcare Devices Limited
 *
 * THIS SOFTWARE IS PROVIDED BY COMFIT HEALTHCARE DEVICES LIMITED "AS IS" AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COMFIT HEALTHCARE DEVICES LIMITED OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.comfithealth.library.models


@Suppress("ArrayInDataClass")
data class Status(
    val state: DeviceState,
    val finishCode: FinishCode?,
    val boomerangErr: BoomerangErr?,
    val isDSPOn: Boolean,
    val fingers: FingerMode,
    val mode: Mode,
    val lpFilter: LPFilter,
    val sampleRate: SampleRate,
    private val value: ByteArray? = null,
) {
    override fun toString(): String {
        return "Status(rawValue=${value?.contentToString()}, state=$state, sampleRate=$sampleRate, lpFilter=$lpFilter, mode=$mode, fingers=$fingers, isDSPOn=$isDSPOn, finishCode=$finishCode, errorCode=$boomerangErr)"
    }
}

