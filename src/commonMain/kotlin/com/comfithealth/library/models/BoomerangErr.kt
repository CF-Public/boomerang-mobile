/*
 * Copyright (c) 2021, Comfit Healthcare Devices Limited
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms will only be
 * allowed with prior written permission from authorized personnel
 * in Comfit Healthcare Devices Limited
 *
 * THIS SOFTWARE IS PROVIDED BY COMFIT HEALTHCARE DEVICES LIMITED "AS IS" AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COMFIT HEALTHCARE DEVICES LIMITED OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.comfithealth.library.models


sealed class BoomerangErr {
    object BleErr : BoomerangErr()
    object LeadOff : BoomerangErr()
    object PllFailure : BoomerangErr()
    object EcgSendQueue : BoomerangErr()
    object EcgFifoEmpty : BoomerangErr()
    object EcgFifoOverflown : BoomerangErr()
    object BiozFifoEmpty : BoomerangErr()
    object BiozFifoOverflown : BoomerangErr()
    object EcgFifoRead : BoomerangErr()
    object InsufficientReadTime : BoomerangErr()
    object TimeoutShutdown : BoomerangErr()
    object ExtendedLeadOffShutdown : BoomerangErr()
    object LowBattery : BoomerangErr()
    object DangerouslyLowBattery : BoomerangErr()
    object Speaker : BoomerangErr()
    object RealTimeClock : BoomerangErr()
    object SensorCalibration : BoomerangErr()
    object MaxRetryExceeded : BoomerangErr()
    class Extended(val code: Int) : BoomerangErr()
    object Unknown : BoomerangErr()
}