/*
 * Copyright (c) 2021, Comfit Healthcare Devices Limited
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms will only be
 * allowed with prior written permission from authorized personnel
 * in Comfit Healthcare Devices Limited
 *
 * THIS SOFTWARE IS PROVIDED BY COMFIT HEALTHCARE DEVICES LIMITED "AS IS" AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COMFIT HEALTHCARE DEVICES LIMITED OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.comfithealth.library.models

/**
 * Boomerang Service Event
 */
sealed class Evt  {
    
    data class Status(val state: DeviceState, val boomerangErr: BoomerangErr?) : Evt()

    
    data class Reset(val config: Config) : Evt()

    /**
     * Stopped recording from app's side.
     */
    
    data class UserStopped(val data: RecordSession) : Evt()

    
    object Connected : Evt()

    
    object Disconnected : Evt()

    
    class Touch(val touchQuality: TouchQuality) : Evt()

    /**
     * Data transfer complete.
     * Filtering still in progress (if DSP is enabled)
     */
    
    object RecordingDone : Evt()

    
    object RecordingFailed : Evt()

    
    data class DelayedSample(val value: Double) : Evt()

    
    data class Sample(val value: Short) : Evt()

    
    data class HeartRate(val data: HeartRateData) : Evt()

    
    data class Pace(val data: Int) : Evt()

    /**
     * Data transfer complete && all filtering complete
     */
    
    data class Finished(val data: RecordSession) : Evt()

    override fun toString(): String = this::class.simpleName!!

}