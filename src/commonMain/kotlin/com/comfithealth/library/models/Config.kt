/*
 * Copyright (c) 2021, Comfit Healthcare Devices Limited
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms will only be
 * allowed with prior written permission from authorized personnel
 * in Comfit Healthcare Devices Limited
 *
 * THIS SOFTWARE IS PROVIDED BY COMFIT HEALTHCARE DEVICES LIMITED "AS IS" AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COMFIT HEALTHCARE DEVICES LIMITED OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.comfithealth.library.models


enum class SampleRate(val value: Int) {
    R_256(256),
    R_512(512)
}

enum class ManualModeDuration(val value: Long) {
    Infinite(-1),

    /**
     * 25 seconds
     */
    Sec25(25),

    /**
     * 3 Minutes
     */
    Min3(3 * 60)
}

/**
 * Low-Pass Filter
 */
enum class LPFilter(val value: Int) {
    F40(40),
    F100(100),
    F150(150),
    Off(-1)
}

/**
 * Operation mode
 */
enum class Mode {
    Auto,
    Manual
}

enum class FingerMode {
    Two,
    Three
}

/**
 * Right Arm / Left Arm swap
 */
enum class ArmSwap {
    /**
     * Default
     */
    NoSwap,
    Swap
}

data class Config(
    val lpFilter: LPFilter,
    val mode: Mode,
    val fingerMode: FingerMode,
    val dspFilter: Boolean,
    val sampleRate: SampleRate,
    val manualModeDuration: ManualModeDuration,
    val armSwap: ArmSwap,
)  {

    companion object {
        val DEFAULT = Config(
            lpFilter = LPFilter.Off,
            mode = Mode.Manual,
            fingerMode = FingerMode.Three,
            dspFilter = false,
            sampleRate = SampleRate.R_256,
            manualModeDuration = ManualModeDuration.Infinite,
            armSwap = ArmSwap.NoSwap
        )
    }
}