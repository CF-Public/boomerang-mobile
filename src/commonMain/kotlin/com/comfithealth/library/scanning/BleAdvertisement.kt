package com.comfithealth.library.scanning

import com.juul.kable.Advertisement

expect class BleAdvertisement(advertisement: Advertisement) {
    val name: String?
    val rssi: Int
    val identifier: String
    internal val advertisement: Advertisement
    override fun equals(other: Any?): Boolean
    override fun hashCode(): Int
    override fun toString(): String
}