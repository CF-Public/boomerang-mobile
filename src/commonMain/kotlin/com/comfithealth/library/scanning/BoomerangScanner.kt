/*
 * Copyright (c) 2021, Comfit Healthcare Devices Limited
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms will only be
 * allowed with prior written permission from authorized personnel
 * in Comfit Healthcare Devices Limited
 *
 * THIS SOFTWARE IS PROVIDED BY COMFIT HEALTHCARE DEVICES LIMITED "AS IS" AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COMFIT HEALTHCARE DEVICES LIMITED OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.comfithealth.library.scanning

import com.github.michaelbull.result.onFailure
import com.github.michaelbull.result.runCatching
import com.juul.kable.Advertisement
import com.juul.kable.Scanner
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter

class BoomerangScanner(private val scope: CoroutineScope, private val permissions: BluetoothPermissions) {
    private val _state = MutableStateFlow<Scan>(Scan.NotStarted)
    val state: StateFlow<Scan> = _state

    private var job: Job? = null

    private val advertisements = mutableListOf<BleAdvertisement>()

    fun startScan(timeoutMillis: Long) {
        if (!permissions.isEnabled()) {
            _state.value = Scan.InsufficientPerm
            return
        }
        job?.cancel()

        advertisements.clear()
        _state.value = Scan.Scanning(advertisements.toList())

        job = scope.launch {
            runCatching {
                withTimeoutOrNull(timeoutMillis) {
                    Scanner().advertisements
                        .filter { it.isBoomerang }
                        .collect { new ->
                            val advertisement = BleAdvertisement(new)
                            when (val idx = advertisements.indexOfFirst { advertisement.identifier == it.identifier }) {
                                -1 -> advertisements.add(advertisement)
                                else -> advertisements[idx] = advertisement
                            }
                            _state.value = Scan.Scanning(advertisements.toList())
                        }
                } ?: run {
                    _state.value = Scan.Stopped(advertisements.toList())
                }
            }.onFailure {
                if (it !is CancellationException) {
                    _state.value = Scan.Error(it)
                }
            }
        }
    }

    fun stopScan() {
        job?.cancel()
        _state.value = Scan.Stopped(advertisements.toList())
    }

    private val Advertisement.isBoomerang
        get() = name?.startsWith("Comf") == true
}