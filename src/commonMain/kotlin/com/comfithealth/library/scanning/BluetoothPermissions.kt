package com.comfithealth.library.scanning

interface BluetoothPermissions {
    fun isEnabled(): Boolean
}