package com.comfithealth.library.scanning

sealed class Scan {
    object InsufficientPerm: Scan()
    data class Error(val cause: Throwable) : Scan()
    object NotStarted : Scan()
    data class Stopped(val advertisements: List<BleAdvertisement>) : Scan()
    data class Scanning(val advertisements: List<BleAdvertisement>) : Scan()

    override fun toString(): String = this::class.simpleName!!
}