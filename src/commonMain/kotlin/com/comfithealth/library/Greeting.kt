package com.comfithealth.library

import com.comfithealth.library.common.Platform


class Greeting {
    fun greeting(): String {
        return "Hello, ${Platform().platform}!"
    }
}
