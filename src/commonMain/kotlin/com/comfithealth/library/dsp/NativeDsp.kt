package com.comfithealth.library.dsp

internal expect class NativeDsp constructor() {
    fun filter(signal: ShortArray): DoubleArray
    fun analyze(signal: ShortArray): DspResult
}