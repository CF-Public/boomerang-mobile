package com.comfithealth.library.dsp

expect class DspResult {
    fun getData(): DoubleArray
    fun isNoisy(): Boolean
}