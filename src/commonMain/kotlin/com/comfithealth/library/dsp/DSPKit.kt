/*
 * Copyright (c) 2021, Comfit Healthcare Devices Limited
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms will only be
 * allowed with prior written permission from authorized personnel
 * in Comfit Healthcare Devices Limited
 *
 * THIS SOFTWARE IS PROVIDED BY COMFIT HEALTHCARE DEVICES LIMITED "AS IS" AND ANY
 * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COMFIT HEALTHCARE DEVICES LIMITED OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.comfithealth.library.dsp

import com.comfithealth.library.codec.EcgValueConverter
import com.comfithealth.library.models.TouchQuality
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.math.absoluteValue


internal class DSPKit {
    private val native = NativeDsp()
    companion object {
        /**
         * Value of 0.15mv in ADC.
         * Value obtained from [EcgValueConverter.milliVoltsToADC].
         */
        private const val B = 98.304

        /**
         * Value of 1.5mv in ADC.
         * Value obtained from [EcgValueConverter.milliVoltsToADC].
         */
        private const val L = 983.04
    }


    suspend fun filter(signal: ShortArray) = withContext(Dispatchers.Default) {
       native.filter(signal)
    }


    suspend fun analyze(signal: ShortArray) = withContext(Dispatchers.Default) {
        native.analyze(signal)
    }

    fun touchQuality(signal: List<Short>, sampleRate: Int, isLeadOff: Boolean): TouchQuality? {
        if (signal.size > sampleRate) {
            val lastSecond = signal.subList(signal.size - sampleRate, signal.size)
            val b = lastSecond.count { it.toInt().absoluteValue > B }
            val l = lastSecond.count { it.toInt().absoluteValue > L }
            val p40 = (0.4 * sampleRate).toInt()
            val p25 = (0.25 * sampleRate).toInt()
            val p10 = (0.1 * sampleRate).toInt()
            return when {
                isLeadOff || l >= p40 || b < p25 -> TouchQuality.Bad
                l <= p10 && b >= p25 -> TouchQuality.Good
                else -> TouchQuality.Acceptable
            }
        }
        return null
    }
}