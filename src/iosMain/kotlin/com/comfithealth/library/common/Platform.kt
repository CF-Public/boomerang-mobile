package com.comfithealth.library.common
import kotlinx.cinterop.cValue
import platform.UIKit.UIDevice
import platform.posix.timespec
import platform.posix.nanosleep

actual class Platform actual constructor() {
    actual val platform: String = UIDevice.currentDevice.systemName() + " " + UIDevice.currentDevice.systemVersion
}

internal actual suspend fun sleep(millis: Long, nanos: Int) {
    val time = cValue<timespec> {
        tv_sec = millis * 1_000
        tv_nsec = nanos.toLong()
    }
    nanosleep(time, null)
}