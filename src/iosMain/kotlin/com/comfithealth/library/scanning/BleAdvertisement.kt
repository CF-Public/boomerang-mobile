package com.comfithealth.library.scanning

import com.juul.kable.Advertisement

actual class BleAdvertisement actual constructor(internal actual val advertisement: Advertisement) {
    actual val name: String?
        get() = advertisement.name
    actual val rssi: Int
        get() = advertisement.rssi
    actual val identifier: String
        get() = advertisement.identifier.toString()

    actual override fun toString(): String {
        return "BleAdvertisement(name=$name, rssi=$rssi, identifier='$identifier')"
    }
}