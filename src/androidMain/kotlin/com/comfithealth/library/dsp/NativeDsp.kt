package com.comfithealth.library.dsp

import com.comfithealth.boomerang.dsp.Dspkit

internal actual class NativeDsp {
    private val delegate = Dspkit()

    actual fun filter(signal: ShortArray): DoubleArray = delegate.filter(signal)

    actual fun analyze(signal: ShortArray): DspResult = delegate.analyze(signal)
}
