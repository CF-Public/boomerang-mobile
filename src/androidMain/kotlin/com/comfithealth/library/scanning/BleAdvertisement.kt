package com.comfithealth.library.scanning

import com.juul.kable.Advertisement

actual class BleAdvertisement actual constructor(internal actual val advertisement: Advertisement) {
    actual val name: String?
        get() = advertisement.name
    actual val rssi: Int
        get() = advertisement.rssi
    actual val identifier: String
        get() = advertisement.address

    actual override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BleAdvertisement

        if (name != other.name) return false
        if (rssi != other.rssi) return false
        if (identifier != other.identifier) return false

        return true
    }

    actual override fun hashCode(): Int {
        var result = name?.hashCode() ?: 0
        result = 31 * result + rssi
        result = 31 * result + identifier.hashCode()
        return result
    }

    actual override fun toString(): String {
        return "BleAdvertisement(name=$name, rssi=$rssi, identifier='$identifier')"
    }


}