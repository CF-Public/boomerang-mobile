package com.comfithealth.library.scanning

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import com.juul.kable.Scanner

class BluetoothPermissionsAndroid(private val context: Context) : BluetoothPermissions {
    companion object {
        val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    }

    override fun isEnabled(): Boolean =
        context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
}
