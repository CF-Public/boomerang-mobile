package com.comfithealth.library.common

actual class Platform actual constructor() {
    actual val platform: String = "Android ${android.os.Build.VERSION.SDK_INT}"
}

@Suppress("BlockingMethodInNonBlockingContext")
actual suspend fun sleep(millis: Long, nanos: Int) {
    Thread.sleep(millis, nanos)
}