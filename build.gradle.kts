plugins {
    kotlin("multiplatform") version "1.5.10"
    id("com.android.library")
//    kotlin("plugin.serialization")  version "1.5.0"
    id("com.google.protobuf") version "0.8.16"
    id("maven-publish")
}

group = "com.comfithealth.boomerang"
version = "1.0.1"

val koinVersion = "3.1.0"

repositories {
    google()
    jcenter()
    mavenCentral()
}

kotlin {
    android {
        publishAllLibraryVariants()
    }
    iosX64 {
        binaries {
            framework {
                baseName = "boomerang"
            }
        }
    }
    iosArm64 {
        binaries {
            framework {
                baseName = "boomerang"
            }
        }
    }


    sourceSets {
        val appleMain by creating {
            kotlin.srcDir("src/iosMain/kotlin")
        }
        val commonMain by getting {
            dependencies {
//                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.2")
                implementation("io.insert-koin:koin-core:$koinVersion")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.2.1")
                implementation("org.jetbrains.kotlinx:kotlinx-io:0.1.11")
                implementation("io.github.aakira:napier:1.5.0")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.2.1")
                api("com.michael-bull.kotlin-result:kotlin-result:1.1.12")
                implementation("com.juul.kable:core:0.5.1")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation("io.insert-koin:koin-test:$koinVersion")
            }
        }
        val androidMain by getting {
            dependencies {
                implementation("com.google.android.material:material:1.2.1")
            }
        }
        val androidTest by getting {
            dependencies {
                implementation("junit:junit:4.13")
            }
        }
        val iosArm64Main by getting {
            kotlin.srcDir("src/iosMain")
        }
        val iosX64Main by getting {
            kotlin.srcDir("src/iosMain")
        }
        val iosArm64Test by getting {
            kotlin.srcDir("src/iosTest")
        }
        val iosX64Test by getting {
            kotlin.srcDir("src/iosTest")
        }
    }
}

android {
    compileSdkVersion(30)
    ndkVersion = "21.4.7075529"
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    defaultConfig {
        minSdkVersion(24)
        targetSdkVersion(30)
        externalNativeBuild {
            cmake {
                arguments(
                    "-DSCAPIX_PLATFORM=android",
                    "-DSCAPIX_BRIDGE=java",
                    "-DSCAPIX_JAVA_API=android-30"
                )
            }
        }
        ndk {
            // By default, all 4 ABIs are built:
            // abiFilters 'x86', 'x86_64', 'armeabi-v7a', 'arm64-v8a'
//            abiFilters.addAll(
//                setOf("x86_64")
//            )
        }
    }

    externalNativeBuild {
        cmake {
            path = file("src/commonMain/native/CMakeLists.txt")
            version = "3.18.1"
        }
    }
    sourceSets.getByName("main") {
        jni.srcDirs("src/commonMain/native/source")
        java.srcDirs("src/commonMain/native/generated/bridge/java")
        // kotlin.srcDir("src/commonMain/native/generated/bridge/java")
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}
buildscript {
    dependencies {
        classpath("com.android.tools.build:gradle:4.1.3")
    }
}
